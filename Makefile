#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/02/25 12:34:27 by cwagner           #+#    #+#              #
#    Updated: 2014/06/24 15:51:42 by cwagner          ###   ########.fr        #
#                                                                              #
#******************************************************************************#


CLIENTNAME	= client
SERVERNAME	= serveur
GFXNAME		= gfx

DEBUG =		no

ifeq ($(DEBUG), yes)
	CC = clang
	CFLAGS = -Wall -Wextra -Werror -pedantic -g3
else
	CC = gcc
	CFLAGS = -Wall -Wextra -Werror
endif


INCLUDES = -I./includes

DCLI	=	dcli/
DSERV	=	dserv/
DGFX	=	dgfx/

SCLI =	check_param.c	\
		client.c

SRCSCLI = $(addprefix $(DCLI), $(SCLI))

SSERV =	broadfunc.c		\
		cofunc.c		\
		eggfunc.c		\
		expulsefunc.c	\
		getfunc.c		\
		getopt.c		\
		getopt_2.c		\
		gfxsend1.c		\
		gfxsend2.c		\
		gfxsend3.c		\
		gfxsend4.c		\
		incafunc.c		\
		incafunc2.c		\
		initmap.c		\
		interpret.c		\
		inventoryfunc.c	\
		mainloop.c		\
		movefunc.c		\
		protocol_tools.c\
		putfunc.c		\
		read.c			\
		seefunc.c		\
		seefunc2.c		\
		server.c		\
		servfunc2.c		\
		srv_accept.c	\
		srv_accept2.c	\
		time_tools.c	\
		tools.c			\
		updater.c		\
		updater2.c		\
		write.c

SRCSSERV = $(addprefix $(DSERV), $(SSERV))

SGFX =	client_loop.c				\
		cmd_edi_sgt_seg_smg_suc.c	\
		cmd_msz_bct_tna_ppo_plv.c	\
		cmd_pdr_pgt_enw_eht_ebo.c	\
		cmd_pex_pbc_pic_pie_pfk.c	\
		command_treatment.c			\
		draw_arms.c					\
		draw_background.c			\
		draw_broadcast.c			\
		draw_eggs.c					\
		draw_endgame.c				\
		draw_incants.c				\
		draw_infos.c				\
		draw_it.c					\
		draw_legs.c					\
		draw_players.c				\
		draw_ressources.c			\
		gfx.c						\
		handle_imgs.c				\
		handle_mlx_build.c			\
		launch_init.c				\
		protocol_gfx.c				\
		tools.c						\
		treat_enw.c					\
		treat_pdi.c					\
		treat_pin.c					\
		treat_pnw.c					\
		x.c

SRCSGFX = $(addprefix $(DGFX), $(SGFX))

CLIENTOBJ = $(SRCSCLI:.c=.o)
SERVEROBJ = $(SRCSSERV:.c=.o)
GFXOBJ = $(SRCSGFX:.c=.o)

LIB = 		libft/
MLX = -L/usr/X11/lib -lmlx -lXext -lX11

all: REMLINK BUILDFT $(GFXNAME) $(SERVERNAME) CREATELINK

ifeq ($(DEBUG), yes)
	@echo
	@echo "\033[33m[Generation]\033[0m mode debug"
	@echo
else
	@echo
	@echo "\033[33m[Generation]\033[0m Mode: \033[32m[Release]\033[0m"
	@echo
endif

BUILDFT:
	@echo "\033[31m[Libft] \033[0m" | tr -d '\n'
	make -C $(LIB)

REMLINK:
	@rm -f client

CREATELINK:
	@ln -s dcli/client.py client
	@chmod +x client

$(CLIENTNAME): $(CLIENTOBJ)
	@echo "\033[32m[Building Client] \033[0m" | tr -d '\n'
	$(CC) $(FLAG) -o $@ $^ -L$(LIB) -lft

$(SERVERNAME): $(SERVEROBJ)
	@echo "\033[32m[Building Server] \033[0m" | tr -d '\n'
	$(CC) $(FLAG) -o $@ $^ -L$(LIB) -lft

$(GFXNAME): $(GFXOBJ)
	@echo "\033[32m[Building Graphic Client] \033[0m" | tr -d '\n'
	$(CC) $(FLAG) -o $@ $^ -L$(LIB) -lft $(MLX)

remake:
	@echo "\033[31m[Re-Libft] \033[0m" | tr -d '\n'
	make re -C $(LIB)

%.o: %.c
	@echo "\033[33m[Doing object] \033[0m" | tr -d '\n'
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ $^

clean:
	@echo "\033[34m[Cleaning] \033[0m" | tr -d '\n'
	rm -f $(CLIENTOBJ) $(SERVEROBJ) $(GFXOBJ) $(SERVGFXOBJ)

fclean: clean
	@echo "\033[34m[Filecleaning] \033[0m" | tr -d '\n'
	rm -f client
	rm -f $(CLIENTNAME) $(SERVERNAME) $(GFXNAME) $(SERVGFXNAME)

ffclean: fclean
	make fclean -C $(LIB)

re: fclean all

rre: remake re

run: re
	./$(SERVERNAME) -p 4242 -x 10 -y 10 -n tutu toto -c 2 -t 100

.PHONY: clean fclean re
