#!/usr/bin/env python
# -*- coding: utf8 -*-
from survive import do_survive

__author__ = 'ggarrigo'
import sys
import socket
import os
import signal
import time
from fonctions import *
from peon import peon
from survive import *
from incant import *
from get_ressource import *
from master_funcs import check_players

cast_queue = []
time_passed = 0


def usage():
	print "Usage : python client.py -p <port> -n <team> [-h <host>]"


def connect_client(port, host):
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	try:
		s.connect((host, port))
	except socket.error:
		print "ERROR: Cannot connect to server."
		sys.exit()
	receive(s)
	sendstr(s, team + "\n")
	tmp = receive(s).split("\n")
	nb_client = int(tmp[0])
	tmp = tmp[1].split()
	map_x = int(tmp[0])
	map_y = int(tmp[1])
	return s, nb_client, map_x, map_y


def check_options(sys):
	# getting options
	flag = 0
	for i in range(1, len(sys.argv)):
		if sys.argv[i] == '-p':
			flag = 1
		elif sys.argv[i] == '-n':
			flag = 2
		elif sys.argv[i] == '-h':
			flag = 3
		elif flag == 1:
			port = int(sys.argv[i])
			flag = 0
		elif flag == 2:
			team = sys.argv[i]
			flag = 0
		elif flag == 3:
			host = sys.argv[i]
			flag = 0
		i += 1
	if 'port' not in locals() or 'team' not in locals():
		usage()
		sys.exit(1)
	if 'host' not in locals():
		host = 'localhost'
	return host, port, team


def handler(signum, frame):
	print 'Interrupted system call.'
	sys.exit()

signal.signal(signal.SIGINT, handler)

if __name__ == '__main__':
	original_sigint = signal.getsignal(signal.SIGINT)
	signal.signal(signal.SIGINT, original_sigint)
	# Récupération des informations et connection
	host, port, team = check_options(sys)
	sock, nb_client, map_x, map_y = connect_client(port, host)

	# On fork jusqu'a avoir le bon nombre de joueurs
	if (nb_client + 1) > 6:
		nb_client = 6
	else:
		nb_client = nb_client + 1
	for i in range(1, nb_client):
		newpid = os.fork()
		if newpid == 0:
			sock.close()
			(sock, nb_client, map_x, map_y) = connect_client(port, host)
			peon(sock, i, team)
			sys.exit(1)


	# On a finis de set les péons
	life = 1260
	fork_timer = 600
	level = 1
	action_queue = []
	survival = 0
	gathering = 1
	call_incant = 2
	comportement = gathering
	toelevate = [{'linemate': 0, 'deraumere': 0, 'sibur': 0, 'mendiane': 0, 'phiras': 0, 'thystame': 0},
				 {'linemate': 1, 'deraumere': 0, 'sibur': 0, 'mendiane': 0, 'phiras': 0, 'thystame': 0},
				 {'linemate': 1, 'deraumere': 1, 'sibur': 1, 'mendiane': 0, 'phiras': 0, 'thystame': 0},
				 {'linemate': 2, 'deraumere': 0, 'sibur': 1, 'mendiane': 0, 'phiras': 2, 'thystame': 0},
				 {'linemate': 1, 'deraumere': 1, 'sibur': 2, 'mendiane': 0, 'phiras': 1, 'thystame': 0},
				 {'linemate': 1, 'deraumere': 2, 'sibur': 1, 'mendiane': 3, 'phiras': 0, 'thystame': 0},
				 {'linemate': 1, 'deraumere': 2, 'sibur': 3, 'mendiane': 0, 'phiras': 1, 'thystame': 0},
				 {'linemate': 2, 'deraumere': 2, 'sibur': 2, 'mendiane': 2, 'phiras': 2, 'thystame': 1},
				 {'linemate': 0}]
	ressource_to_find_by_level = [["linemate"],
								  ["linemate", "deraumere", "sibur"],
								  ["linemate", "sibur", "phiras"],
								  ["linemate", "deraumere", "sibur", "phiras"],
								  ["linemate", "deraumere", "sibur", "mendiane"],
								  ["linemate", "deraumere", "sibur", "phiras"],
								  ["linemate", "deraumere", "sibur", "mendiane", "phiras", "thystame"],
								  ["linemate"]]
	team_inv = {'linemate': 0, 'deraumere': 0, 'sibur': 0, 'mendiane': 0, 'phiras': 0, 'thystame': 0}
	ressources = list(ressource_to_find_by_level[level - 1])

	while sock:
		if level == 8:
			print "Team "+team+" Level 8, game over."
			time.sleep(2)
			sys.exit(1)

		nb_client = check_players(sock, action_queue, cast_queue, nb_client, port, host, team)
		do_survive(sock, action_queue, cast_queue, team, 15, 20)

		# Gestion de la cast_queue
		while cast_queue:
			cast = cast_queue.pop(0)
			split = cast.split(',')
			part1 = split[0].split()
			if part1[0] != "message":
				continue
			part2 = split[1].split()
			if part2[0] != team:
				continue
			if part2[1] == "took":
				team_inv[part2[2]] += 1
				if check_inv_for_incant(team_inv, toelevate[level]):
					comportement = call_incant
				if team_inv[part2[2]] >= toelevate[level][part2[2]]:
					ret = broadcast(sock, cast_queue, team, "enough " + part2[2])
					if part2[2] in ressources:
						ressources.remove(part2[2])


		# Si on doit chercher des pierres
		if comportement == call_incant:
			nb_ready = 1
			ret = broadcast(sock, cast_queue, team, "incantation")
			inv = get_inv(sock, cast_queue)
			for item, nb in inv.items():
				if item == "player" or item == "nourriture":
					continue
				nb = int(nb)
				while nb > 0:
					sendstr(sock, "pose " + item + "\n")
					ret = handle_recv(sock, cast_queue)
					nb -= 1
			while True:
				ret = broadcast(sock, cast_queue, team, "incantation")
				while cast_queue:
					cast = cast_queue.pop(0)
					cast = cast.split(',')
					if not cast[1] or cast[0].split()[0] != "message":
						continue
					cast = cast[1].split()
					if cast[0] == team and cast[1] == "ready":
						nb_ready += 1
				if nb_ready == nb_client:
					sendstr(sock, "voir\n")
					ret = handle_recv(sock, cast_queue)
					ret = ret.split(",")
					sendstr(sock, "incantation\n")
					ret = handle_recv(sock, cast_queue)
					ret = handle_recv(sock, cast_queue)
					ret = ret.split(':')
					if ret[0] == "niveau actuel ":
						level = int(ret[1])
					comportement = gathering
					cast_queue = []
					team_inv = {'linemate': 0, 'deraumere': 0, 'sibur': 0, 'mendiane': 0, 'phiras': 0, 'thystame': 0}
					ressources = list(ressource_to_find_by_level[level - 1])
					break
