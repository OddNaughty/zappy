__author__ = 'sbuono'
import sys


def receive(sock):
	message = sock.recv(512)
	str = "" + message
	if len(message) > 0:
		while len(message) == 512:
			message = sock.recv(512)
			str += message
	else:
		sys.exit(0)
	return str


def sendstr(sock, str) :
	sock.sendall(str)
	return True


def get_case(see, i):
	case = {'nourriture': 0, 'linemate': 0, 'deraumere': 0, 'sibur': 0, 'mendiane': 0, 'phiras': 0, 'thystame': 0,
			'joueur': 0}
	items = see[i].split()
	for item in items:
		case[item] += 1
	return case


def handle_recv(sock, cast_queue):
	ret = ""
	while ret == "":
		str = receive(sock)
		if not str:
			sys.exit(1)
		split = str.split('\n')
		for cmd in split:
			if cmd != '':
				args = cmd.split()
				if args[0] == 'message':
					cast_queue.append(cmd)
				elif split[0] == 'mort':
					print 'I AM DEAD :('
					sys.exit(1)
				else:
					ret = cmd
	return ret


def get_inv(sock, cast_queue):
	sock.send("inventaire\n")
	ret = ""
	while not ret or ret[0] != '{' :
		ret = handle_recv(sock, cast_queue)
	inv = {'nourriture': 0, 'linemate': 0, 'deraumere': 0, 'sibur': 0, 'mendiane': 0, 'phiras': 0, 'thystame': 0}
	str = ret[1:-1]
	str = str.split(',')
	for item in str:
		item = item.split()
		inv[item[0]] = item[1]
	return inv


def get_vision(sock, cast_queue):
	sock.send("voir\n")
	ret = handle_recv(sock, cast_queue)
	see = ret[1:-1]
	see = see.split(',')
	return see


def broadcast(sock, cast_queue, team, str):
	sendstr(sock, 'broadcast ' + team + " " + str + "\n")
	ret = handle_recv(sock, cast_queue)
	return ret
