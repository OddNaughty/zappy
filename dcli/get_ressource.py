__author__ = 'sbuono'
from fonctions import *
import random

def get_tile_to_reach(see, ressources):
	for i in range(len(see)):
		case = get_case(see, i)
		for r in ressources:
			if case[r] > 0:
				return i, r
	return -1, False


def inqueue_avance(action_queue):
	action_queue.append("avance\n")


def inqueue_prendre(action_queue, str):
	action_queue.append("prend " + str + "\n")


def inqueue_gauche(action_queue):
	action_queue.append("gauche\n")


def inqueue_droite(action_queue):
	action_queue.append("droite\n")


def get_ressources(see, action_queue, ressources, cast_queue, sock):
	(casenb, ress) = get_tile_to_reach(see, ressources)
	if casenb == -1:
		return False
	if casenb > 0 :
		casescentre = [0, 2, 6, 12, 20, 30, 42, 56, 72, 90]
		for i in range(len(casescentre) - 1):
			if casescentre[i] <= casenb < casescentre[i + 1]:
				if (casenb - casescentre[i]) < (casescentre[i + 1] - casenb):
					centre = casescentre[i]
				else:
					centre = casescentre[i + 1]
				break
		else:
			return "erreur premier for"
		inv = get_inv(sock, cast_queue)
		for i in casescentre:
			if i <= centre:
				if i > 0 :
					inqueue_avance(action_queue)
				curcase = get_case(see, i)
				if curcase["nourriture"] > 0 and inv["nourriture"] < 13:
					for i in range(curcase["nourriture"]):
						inqueue_prendre(action_queue, "nourriture")
			else:
				break
		if casenb == centre:
			inqueue_prendre(action_queue, ress)
			return action_queue
		elif casenb < centre:
			inqueue_gauche(action_queue)
			dist = centre - casenb
		else:
			inqueue_droite(action_queue)
			dist = casenb - centre
		for i in range(dist):
			inqueue_avance(action_queue)
			if casenb < centre:
				curcase = get_case(see, centre - (i + 1))
			else:
				curcase = get_case(see, centre + i + 1)
			if curcase["nourriture"] > 0 and inv["nourriture"] < 13:
				for j in range(curcase["nourriture"]):
					inqueue_prendre(action_queue, "nourriture")
		inqueue_prendre(action_queue, ress)
	else :
		curcase = get_case(see, 0)
		if curcase["nourriture"] > 0:
			for i in range(curcase["nourriture"]):
				inqueue_prendre(action_queue, "nourriture")
		if ressources != "nourriture" :
			inqueue_prendre(action_queue, ress)
	return action_queue


def gather(sock, action_queue, cast_queue, team, ressources, see):
	get_ressources(see, action_queue, ressources, cast_queue, sock)
	if action_queue == []:
		i = random.randint(1, 2)
		if i == 1:
			inqueue_droite(action_queue)
		else:
			inqueue_gauche(action_queue)
		inqueue_avance(action_queue)
	while action_queue :
		action = action_queue.pop(0)
		sendstr(sock, action)
		str = handle_recv(sock, cast_queue)
		action = action.split()
		if action[0] == "prend" and str == "ok" and action[1] != 'nourriture':
			str = "took " + action[1]
			broadcast(sock, cast_queue, team, str)
