__author__ = 'sbuono'
from fonctions import *


def check_inv_for_incant(team_inv, toelevate) :
	for item, number in team_inv.items() :
		if number < toelevate[item] :
			return False
	return True


def wait_for_a_call(sock, team, id):
	while True:
		str = ""
		lol = receive(sock)
		lol = lol.split('\n')
		for i in lol:
			if "message" in i:
				str = i
		if str == "":
			continue
		split = str.split(',')
		part1 = split[0].split()
		part2 = split[1].split()
		if part1[0] == "message" and part2[0] == team and part2[1] == "incantation":
			return int(part1[1])


def reach_incant(sock, team, cast_queue, id):
	while True:
		direction = wait_for_a_call(sock, team, id)
		if direction == 0:
			return 1
		if direction == 1 or direction == 2 or direction == 8:
			# print 'AVANCE'
			sendstr(sock, "avance\n")
			handle_recv(sock, cast_queue)
		elif direction == 3 or direction == 4:
			# print 'LEFT'
			sendstr(sock, "gauche\n")
			handle_recv(sock, cast_queue)
		elif direction == 7 or direction == 6:
			# print 'RIGHT'
			sendstr(sock, "droite\n")
			handle_recv(sock, cast_queue)
		else:
			# print 'DEMI TOUR'
			sendstr(sock, "droite\ndroite\n")
			handle_recv(sock, cast_queue)
			handle_recv(sock, cast_queue)
	return False


def wait_incant(sock, cast_queue, team):
	inv = get_inv(sock, cast_queue)
	for item, nb in inv.items():
		if item == "nourriture":
			continue
		nb = int(nb)
		while nb > 0:
			sendstr(sock, "pose " + item + "\n")
			ret = handle_recv(sock, cast_queue)
			nb -= 1
	broadcast(sock, cast_queue, team, "ready")
	while True:
		str = receive(sock)
		str = str.split(':')
		if str[0] == "niveau actuel ":
			return int(str[1])

