__author__ = 'ggarrigo'
from fonctions import *
from survive import *
from peon import peon
import socket
import os


#probeme d'include a gerer, ca explique cette fonction degueulasse
def connect_client2(port, host, team):
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((host, port))
	receive(s)
	sendstr(s, team + "\n")
	tmp = receive(s).split("\n")
	tmp = tmp[1].split()
	return s


def check_players(sock, action_queue, cast_queue, nb_client, port, host, team):
	nb_player = nb_client
	id = nb_client
	while nb_client < 6:
		sendstr(sock, 'fork\n')
		handle_recv(sock, cast_queue)
		nb_client += 1
	while nb_player < 6:
		sendstr(sock, 'connect_nbr\n')
		ret = int(handle_recv(sock, cast_queue))
		while ret > 0:
			nb_player += 1
			newpid = os.fork()
			if newpid == 0:
				sock.close()
				sock = connect_client2(port, host, team)
				peon(sock, id, team)
				exit()
			id += 1
			ret -= 1
		do_survive(sock, action_queue, cast_queue, team, 8, 12)
	return nb_client