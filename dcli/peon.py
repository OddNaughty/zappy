#!/usr/bin/env python
# -*- coding: utf8 -*-
__author__ = 'ggarrigo'
from survive import *
from get_ressource import *
from incant import *

def peon(sock, id, team):
	ressource_to_find_by_level = [["linemate"],
							["linemate", "deraumere", "sibur"],
							["linemate", "sibur", "phiras"],
							["linemate", "deraumere", "sibur", "phiras"],
							["linemate", "deraumere", "sibur", "mendiane"],
							["linemate", "deraumere", "sibur", "phiras"],
							["linemate", "deraumere", "sibur", "mendiane", "phiras", "thystame"],
							["linemate"]]

	gathering = 1
	reach_inc = 2
	wait_inc = 3
	level = 1
	cast_queue = []
	action_queue = []
	comportement = gathering
	ressources = list(ressource_to_find_by_level[level - 1])
	while level < 8:
		do_survive(sock, action_queue, cast_queue, team, 10, 15)
		#on effectue une action suivant le comportement
		while cast_queue:
			cast = cast_queue.pop(0)
			split = cast.split(',')
			part1 = split[0].split()
			if part1[0] != "message":
				continue
			part2 = split[1].split()
			if part2[0] != team:
				continue
			if part2[1] == "incantation":
				if part1[1] == 0:
					comportement = wait_inc
				else:
					comportement = reach_inc
			if part2[1] == "enough":
				if part2[2] in ressources:
					ressources.remove(part2[2])

		# Si on doit chercher des pierres
		if comportement == gathering:
			see = get_vision(sock, cast_queue)
			gather(sock, action_queue, cast_queue, team, ressources, see)
		if comportement == wait_inc:
			level = wait_incant(sock, cast_queue, team)
			if int(level) == 8:
				sys.exit(1)
			cast_queue = []
			ressources = list(ressource_to_find_by_level[level - 1])
			sendstr(sock, "avance\n")
			handle_recv(sock, cast_queue)
			comportement = gathering
		if comportement == reach_inc:
			ret = reach_incant(sock, team, cast_queue, id)
			if ret == 1:
				comportement = wait_inc