#!/usr/bin/env python
# -*- coding: utf8 -*-
__author__ = 'ggarrigo'
from fonctions import *
from get_ressource import *
import random

def do_survive(sock, action_queue, cast_queue, team, min_life, max_life):
	# on veux connaitre sa vie actuelle
	inv = get_inv(sock, cast_queue)
	food = int(inv["nourriture"])
	if food > min_life:
		return
	while food < max_life:
		see = get_vision(sock, cast_queue)
		gather(sock, action_queue, cast_queue, team, ["nourriture"], see)
		inv = get_inv(sock, cast_queue)
		food = int(inv["nourriture"])
	return