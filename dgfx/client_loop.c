/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_loop.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/09 12:06:23 by rgary             #+#    #+#             */
/*   Updated: 2014/06/09 12:06:23 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

void	set_env(t_env *e, int sfd)
{
	e->max = sfd + 1;
	e->sfd = sfd;
	e->gameover = 0;
	e->zoom = 1;
	e->screen_width = (int)((float)SCREEN_WIDTH * e->zoom);
	e->screen_height = (int)((float)SCREEN_HEIGHT * e->zoom);
	e->img_posx = 0;
	e->img_posy = 0;
	e->move.move_up = 0;
	e->move.move_down = 0;
	e->move.move_left = 0;
	e->move.move_right = 0;
}

void	main_gfx_loop(int sfd)
{
	t_env	e;

	set_env(&e, sfd);
	first_init(&e, sfd);
	if (create_mlx(&e) == -1)
		return ;
}
