/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_edi_sgt_seg_smg_suc.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/16 06:13:43 by rgary             #+#    #+#             */
/*   Updated: 2014/06/16 06:13:43 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

void	treat_edi(t_env *e, char *cmd)
{
	char	**args;
	t_clist	*save;

	args = ft_strsplit(cmd, ' ');
	save = e->clist;
	while (save)
	{
		if (save->egg_nb == ft_atoi(args[1]))
		{
			save->egg_state[0] = EGG_DEAD;
			save->egg_state[1] = time(NULL) + ANIMTIME;
		}
		save = save->next;
	}
	free_args(args);
}

void	treat_sgt(t_env *e, char *cmd)
{
	char	**args;

	args = ft_strsplit(cmd, ' ');
	e->time_unit = ft_atoi(args[1]);
	free_args(args);
}

void	treat_seg(t_env *e, char *cmd)
{
	char	**args;

	args = ft_strsplit(cmd, ' ');
	e->winner = ft_strdup(args[1]);
	e->gameover = 1;
	(void)cmd;
}

void	treat_smg(t_env *e, char *cmd)
{
	char	**args;

	(void)e;
	args = ft_strsplit(cmd, ' ');
	ft_putstr(args[1]);
	free_args(args);
}

void	treat_suc(t_env *e, char *cmd)
{
	(void)e;
	(void)cmd;
}
