/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_msz_bct_tna_ppo_plv.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/16 06:10:00 by rgary             #+#    #+#             */
/*   Updated: 2014/06/16 06:10:00 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

void	treat_msz(t_env *e, char *cmd)
{
	int		i;
	int		j;
	char	*tmp;

	i = 4;
	j = 0;
	while (cmd[i + j] >= '0' && cmd[i + j] <= '9')
		j++;
	tmp = ft_strsub(cmd, i, j);
	free(tmp);
	e->mszy = ft_atoi(tmp);
	i += j;
	while (cmd[i] < '0' || cmd[i] > '9')
		i++;
	while (cmd[i + j] >= '0' && cmd[i + j] <= '9')
		j++;
	tmp = ft_strsub(cmd, i, j);
	e->mszx = ft_atoi(tmp);
	free(tmp);
	fill_res(e);
}

void	treat_bct(t_env *e, char *cmd)
{
	int		i;
	char	**args;

	i = -1;
	args = ft_strsplit(cmd, ' ');
	if (ft_atoi(args[2]) > e->mszx || ft_atoi(args[1]) > e->mszy)
		return ;
	while (i++, i < NB_RESS)
		e->res[ft_atoi(args[2])][ft_atoi(args[1])][i] = ft_atoi(args[i + 3]);
	free_args(args);
}

void	treat_tna(t_env *e, char *cmd)
{
	(void)e;
	(void)cmd;
}

void	treat_ppo(t_env *e, char *cmd)
{
	char	**args;
	t_clist	*save;

	args = ft_strsplit(cmd, ' ');
	save = e->clist;
	if (ft_atoi(args[3]) >= e->mszx || ft_atoi(args[2]) >= e->mszy)
		return ;
	if (save)
	{
		while (save)
		{
			if (save->client_nb == ft_atoi(args[1]))
			{
				save->posx = ft_atoi(args[3]);
				save->posy = ft_atoi(args[2]);
				save->ori = ft_atoi(args[4]);
				break ;
			}
			save = save->next;
		}
	}
	free_args(args);
}

void	treat_plv(t_env *e, char *cmd)
{
	char	**args;
	t_clist	*save;

	args = ft_strsplit(cmd, ' ');
	save = e->clist;
	if (save)
	{
		while (save)
		{
			if (save->client_nb == ft_atoi(args[1]))
			{
				save->lvl = ft_atoi(args[2]);
				break ;
			}
			save = save->next;
		}
	}
	free_args(args);
}
