/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_pdr_pgt_enw_eht_ebo.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/16 06:12:22 by rgary             #+#    #+#             */
/*   Updated: 2014/06/16 06:12:22 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

void	treat_pdr(t_env *e, char *cmd)
{
	(void)e;
	(void)cmd;
}

void	treat_pgt(t_env *e, char *cmd)
{
	(void)e;
	(void)cmd;
}

void	treat_eht(t_env *e, char *cmd)
{
	char	**args;
	t_clist	*save;

	args = ft_strsplit(cmd, ' ');
	save = e->clist;
	while (save)
	{
		if (save->egg_nb == ft_atoi(args[1]))
		{
			save->egg_state[0] = EGG_BROKEN;
			save->egg_state[1] = time(NULL) + ANIMTIME;
		}
		save = save->next;
	}
	free_args(args);
}

void	treat_ebo(t_env *e, char *cmd)
{
	char	**args;
	t_clist	*save;

	args = ft_strsplit(cmd, ' ');
	save = e->clist;
	while (save)
	{
		if (save->egg_nb == ft_atoi(args[1]))
		{
			if (save->prev)
				save->prev->next = save->next;
			else
				e->clist = save->next;
			if (save->next)
				save->next->prev = save->prev;
			free(save->teamname);
			free(save);
			break ;
		}
		save = save->next;
	}
	free_args(args);
}
