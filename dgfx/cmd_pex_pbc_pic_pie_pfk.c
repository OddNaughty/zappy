/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_pex_pbc_pic_pie_pfk.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/16 06:11:04 by rgary             #+#    #+#             */
/*   Updated: 2014/06/16 06:11:04 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

void	treat_pex(t_env *e, char *cmd)
{
	(void)e;
	(void)cmd;
}

void	treat_pbc(t_env *e, char *cmd)
{
	char	**args;
	t_clist	*save;

	args = ft_strsplit(cmd, ' ');
	save = e->clist;
	while (save)
	{
		if (ft_atoi(args[1]) == save->client_nb)
		{
			save->bc[0] = 1;
			save->bc[1] = time(NULL) + ANIMTIME;
			break ;
		}
		save = save->next;
	}
	(void)e;
	(void)cmd;
}

void	treat_pic(t_env *e, char *cmd)
{
	char	**args;

	args = ft_strsplit(cmd, ' ');
	if (ft_atoi(args[2]) >= e->mszx || ft_atoi(args[1]) >= e->mszy)
		return ;
	e->res[ft_atoi(args[2])][ft_atoi(args[1])][INCANT] = 1;
}

void	treat_pie(t_env *e, char *cmd)
{
	char	**as;

	as = ft_strsplit(cmd, ' ');
	if (ft_atoi(as[2]) >= e->mszx || ft_atoi(as[1]) >= e->mszy)
		return ;
	e->res[ft_atoi(as[2])][ft_atoi(as[1])][INCANT] = 0;
	e->res[ft_atoi(as[2])][ft_atoi(as[1])][RESINC] = ft_atoi(as[3]) + 8;
	e->res[ft_atoi(as[2])][ft_atoi(as[1])][ENDRESINC] = time(NULL) + ANIMTIME;
	free_args(as);
}

void	treat_pfk(t_env *e, char *cmd)
{
	(void)e;
	(void)cmd;
}
