/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command_treatment.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/09 02:47:58 by rgary             #+#    #+#             */
/*   Updated: 2014/06/09 02:47:58 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

void	free_args(char **args)
{
	int	i;

	i = -1;
	while (i++, args[i])
		free(args[i]);
	free(args);
}

void	fill_res(t_env *e)
{
	int	i;
	int	j;
	int	k;

	e->res = (int***)malloc(sizeof(int**) * e->mszx);
	i = -1;
	while (i++, i < e->mszx)
	{
		e->res[i] = (int**)malloc(sizeof(int*) * e->mszy);
		j = -1;
		while (j++, j < e->mszy)
			e->res[i][j] = (int*)malloc(sizeof(int) * (NB_C_INFO));
	}
	i = -1;
	while (i++, i < e->mszx)
	{
		j = -1;
		while (j++, j < e->mszy)
		{
			k = -1;
			while (k++, k < NB_C_INFO)
				e->res[i][j][k] = 0;
		}
	}
}

void	treat_sbp(t_env *e, char *cmd)
{
	(void)e;
	(void)cmd;
}
