/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_background.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/12 11:01:41 by rgary             #+#    #+#             */
/*   Updated: 2014/06/12 11:01:41 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

int		draw_horizontal_line(int i, t_env *e)
{
	static int	color = CASE_LINE;

	while (i++, i % e->mlx->TLN != 0)
		ft_memcpy(e->mlx->draw + (i * 4), &color, e->mlx->color_byte_size);
	i += e->mlx->total_line_size;
	return (i);
}

void	draw_vertical_line(int i, t_env *e)
{
	static int	color = CASE_LINE;

	ft_memcpy(e->mlx->draw + (i * 4), &color, e->mlx->color_byte_size);
}

void	draw_grass(int i, t_env *e)
{
	int	color;
	int	randval;

	randval = rand();
	if ((randval % 15) == 0)
		color = 0x00395D33;
	else if ((randval % 8) == 0)
		color = 0x00526F35;
	else if ((randval % 10) == 0)
		color = 0x00659D32;
	else if ((randval % 6) == 0)
		color = 0x00567E3A;
	else
		color = 0x004DBD33;
	ft_memcpy(e->mlx->draw + (i * 4), &color, e->mlx->color_byte_size);
}

void	draw_background(t_env *e)
{
	int				i;
	static char		*bg = NULL;
	static float	zoom = 0;

	if (i = -1, bg == NULL || zoom != e->zoom)
	{
		if ((zoom = e->zoom), bg != NULL)
			free(bg);
		bg = (char*)malloc(sizeof(char) * e->mlx->size_line * e->screen_height);
		while (i++, i < e->mlx->total_line_size * e->screen_height)
		{
			if (i % (e->mlx->TLN * (e->screen_height / e->mszx)) == 0)
				i = draw_horizontal_line(i, e);
			else
			{
				if (((i % (e->mlx->TLN)) % (e->screen_width / e->mszy)) == 0)
					draw_vertical_line(i, e);
				else
					draw_grass(i, e);
			}
		}
		ft_memcpy(bg, e->mlx->draw, e->mlx->size_line * e->screen_height);
	}
	else
		ft_memcpy(e->mlx->draw, bg, e->mlx->size_line * e->screen_height);
}
