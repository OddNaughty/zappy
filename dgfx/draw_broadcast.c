/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_broadcast.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/18 11:43:46 by rgary             #+#    #+#             */
/*   Updated: 2014/06/18 11:43:46 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

void	draw_broadcast(t_env *e)
{
	t_clist			*save;
	t_timeval		timeval;

	gettimeofday(&timeval, NULL);
	save = e->clist;
	while (save)
	{
		if (save->bc[0] && time(NULL) < save->bc[1])
		{
			if (timeval.tv_usec / 100000 > 4)
				draw_incant(e, save->posx, save->posy, BDCT);
		}
		save = save->next;
	}
}
