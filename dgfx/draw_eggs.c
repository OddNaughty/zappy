/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_eggs.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/17 12:30:37 by rgary             #+#    #+#             */
/*   Updated: 2014/06/17 12:30:37 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

void	draw_eggs(t_env *e)
{
	t_clist	*save;

	save = e->clist;
	while (save)
	{
		if (save->egg_nb == 0)
		{
			save = save->next;
			continue ;
		}
		if (save->egg_state[0] == EGG_OK)
			draw_incant(e, save->posx, save->posy, EGG);
		else if (save->egg_state[0] == EGG_BROKEN && time(NULL) < SES[1])
			draw_incant(e, save->posx, save->posy, BEGG);
		else if (save->egg_state[0] == EGG_DEAD && time(NULL) < SES[1])
			draw_incant(e, save->posx, save->posy, DEGG);
		save = save->next;
	}
}
