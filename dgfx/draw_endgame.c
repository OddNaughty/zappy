/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_endgame.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/17 04:45:12 by rgary             #+#    #+#             */
/*   Updated: 2014/06/17 04:45:12 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

void	draw_winner(t_env *e)
{
	int			i;
	char		*s[2];
	t_clist		*save;

	i = 15;
	save = e->clist;
	while (save)
	{
		if (save->client_nb == 0)
		{
			save = save->next;
			continue ;
		}
		i += 15;
		asprintf(&s[0], PL3, save->client_nb, save->lvl, save->posy,
					save->posx, fill_ori(save), save->inv[0], save->inv[1],
					save->inv[2], save->inv[3], save->inv[4], save->inv[5],
					save->inv[6], save->teamname);
		mlx_string_put(e->mlx->mlx, e->mlx->win2, 5, i, WHITE, s[0]);
		save = save->next;
		free(s[0]);
	}
	i += 15;
	asprintf(&s[0], "%s won the game !", e->winner);
	MSP(MLX->mlx, MLX->win2, INFO_WIDTH / 2 - STR_WIN_LEN, i, WHITE, s[0]);
}

void	draw_endgame(t_env *e)
{
	int			i[3];
	float		r[2];
	int			sq[2];
	int			imgs[2];
	static int	stat = 0;

	sq[0] = (e->screen_width / 12) * 10;
	sq[1] = (e->screen_height / 12) * 10;
	r[0] = (float)e->mlx->xpm_w[ENDGAME] / (float)sq[0];
	r[1] = (float)e->mlx->xpm_h[ENDGAME] / (float)sq[1];
	imgs[1] = ((ESH / 12) * e->mlx->TLN) + (ESW / 12) + (stat % ESW);
	i[0] = -1;
	while (i[0]++, (sq[1] - (sq[1] / 2) + i[0]) < (sq[1] + (sq[1] / 2)))
	{
		i[1] = -1;
		while (i[1]++, (sq[0] - (sq[0] / 2) + i[1]) < (sq[0] + (sq[0] / 2)))
		{
			i[2] = imgs[1] + (i[0] * e->mlx->TLN) + i[1];
			ft_memcpy(&imgs[0], MLX->xpm[ENDGAME] + (((int)((float)i[0] * r[1])
				* MLX->xpm_w[ENDGAME]) + (int)((float)i[1] * r[0])) * 4, 4);
			if (imgs[0] != 0)
				ft_memcpy(e->mlx->draw + (i[2] * 4), &imgs[0], e->mlx->CBS);
		}
	}
	stat += 10;
}
