/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_incants.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/14 10:31:36 by rgary             #+#    #+#             */
/*   Updated: 2014/06/14 10:31:36 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

void	draw_incants(t_env *e)
{
	int		i;
	int		j;

	i = -1;
	while (i++, i < e->mszx)
	{
		j = -1;
		while (j++, j < e->mszy)
		{
			if (e->res[i][j][INCANT] > 0)
				draw_incant(e, i, j, 7);
			if (time(NULL) < e->res[i][j][ENDRESINC])
			{
				if (e->res[i][j][RESINC] == 8 || e->res[i][j][RESINC] == 9)
					draw_incant_result(e, i, j, e->res[i][j][RESINC]);
			}
			if (e->dead[0] == i && e->dead[1] == j && time(NULL) < e->dead[2])
				draw_incant(e, i, j, BLOOD);
		}
	}
}

void	draw_incant(t_env *e, int line, int j, int c)
{
	int		i[3];
	float	r[2];
	int		sq[2];
	int		img;
	int		start;

	sq[1] = ((e->screen_width / e->mszx) / 12) * 10;
	sq[0] = ((e->screen_height / e->mszy) / 12) * 10;
	r[0] = (float)e->mlx->xpm_w[c] / (float)sq[0];
	r[1] = (float)e->mlx->xpm_h[c] / (float)sq[1];
	start = (line * (ESW / e->mszx) * e->mlx->TLN) + (j * (ESH / e->mszy)) +
		(((ESH / e->mszy) / 12) * e->mlx->TLN) + (((ESW / e->mszx) / 12));
	i[0] = -1;
	while (i[0]++, (sq[1] - (sq[1] / 2) + i[0]) < (sq[1] + (sq[1] / 2)))
	{
		i[1] = -1;
		while (i[1]++, (sq[0] - (sq[0] / 2) + i[1]) < (sq[0] + (sq[0] / 2)))
		{
			i[2] = start + (i[0] * e->mlx->total_line_size) + i[1];
			ft_memcpy(&img, e->mlx->xpm[c] + (((int)((float)i[0] * r[1]) *
						e->mlx->xpm_w[c]) + (int)((float)i[1] * r[0])) * 4, 4);
			if (img != 0)
				ft_memcpy(e->mlx->draw + (i[2] * 4), &img, e->mlx->CBS);
		}
	}
}

void	draw_incant_result(t_env *e, int line, int j, int c)
{
	int		i[3];
	float	r[2];
	int		sq[2];
	int		img;
	int		start;

	sq[1] = ((e->screen_width / e->mszx) / 12) * 10;
	sq[0] = ((e->screen_height / e->mszy) / 12) * 10;
	r[0] = (float)e->mlx->xpm_w[c] / (float)sq[0];
	r[1] = (float)e->mlx->xpm_h[c] / (float)sq[1];
	start = (line * (ESW / e->mszx) * e->mlx->TLN) + (j * (ESH / e->mszy)) +
		(((ESH / e->mszy) / 12) * e->mlx->TLN) + (((ESW / e->mszx) / 12));
	i[0] = -1;
	while (i[0]++, (sq[1] - (sq[1] / 2) + i[0]) < (sq[1] + (sq[1] / 2)))
	{
		i[1] = -1;
		while (i[1]++, (sq[0] - (sq[0] / 2) + i[1]) < (sq[0] + (sq[0] / 2)))
		{
			i[2] = start + (i[0] * e->mlx->total_line_size) + i[1];
			ft_memcpy(&img, e->mlx->xpm[c] + (((int)((float)i[0] * r[1]) *
						e->mlx->xpm_w[c]) + (int)((float)i[1] * r[0])) * 4, 4);
			if (img != 0)
				ft_memcpy(e->mlx->draw + (i[2] * 4), &img, e->mlx->CBS);
		}
	}
}
