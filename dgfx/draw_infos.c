/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_infos.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/14 10:35:35 by rgary             #+#    #+#             */
/*   Updated: 2014/06/14 10:35:35 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

void	reset_page(t_env *e)
{
	int			i;
	int			j;
	static int	k = 0;
	static int	color = 0x00000000;

	if (k == 0)
	{
		i = -1;
		while (i++, i < INFO_HEIGHT)
		{
			j = -1;
			while (j++, j < INFO_WIDTH)
				ft_memcpy(e->mlx->fdraw + (i * MLX->SILI + j), &color, 4);
		}
		k = 687322;
	}
	mlx_put_image_to_window(MLX->mlx, MLX->win2, MLX->fimg, 0, 0);
}

int		fill_ori(t_clist *save)
{
	if (save->ori == 1)
		return ('N');
	if (save->ori == 2)
		return ('E');
	if (save->ori == 3)
		return ('S');
	if (save->ori == 4)
		return ('W');
	return ('0');
}

void	draw_infos(t_env *e)
{
	int			i;
	char		*str[2];
	t_clist		*save;

	i = 15;
	asprintf(&str[0], INFO3, "NBR");
	reset_page(e);
	mlx_string_put(e->mlx->mlx, e->mlx->win2, 5, 15, 0x00ffffff, str[0]);
	save = e->clist;
	while (save)
	{
		if (save->client_nb == 0)
		{
			save = save->next;
			continue ;
		}
		i += 15;
		asprintf(&str[0], PL3, save->client_nb, save->lvl, save->posy,
					save->posx, fill_ori(save), save->inv[0], save->inv[1],
					save->inv[2], save->inv[3], save->inv[4], save->inv[5],
					save->inv[6], save->teamname);
		mlx_string_put(e->mlx->mlx, e->mlx->win2, 5, i, WHITE, str[0]);
		save = save->next;
		free(str[0]);
	}
}
