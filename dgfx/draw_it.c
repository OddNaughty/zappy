/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_it.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/13 10:15:16 by rgary             #+#    #+#             */
/*   Updated: 2014/06/13 10:15:16 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

void	draw_it(t_env *e)
{
	ft_bzero(e->mlx->draw, (e->mlx->size_line * e->screen_height));
	draw_background(e);
	draw_ressources(e);
	draw_players(e);
	draw_eggs(e);
	draw_broadcast(e);
	draw_incants(e);
	if (e->gameover == 1)
	{
		draw_infos(e);
		draw_endgame(e);
		draw_winner(e);
	}
	mlx_put_image_to_window(e->mlx->mlx, e->mlx->win, e->mlx->img, e->img_posx,
							e->img_posy);
}
