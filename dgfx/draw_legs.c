/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_legs.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/12 05:14:25 by rgary             #+#    #+#             */
/*   Updated: 2014/06/12 05:14:25 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

void	draw_ls_legs(t_env *e, int i, int j, int color)
{
	int		start;
	int		sq_sz[2];
	int		sq_l[2];
	int		index;

	sq_l[0] = e->screen_width / e->mszy;
	sq_l[1] = e->screen_height / e->mszx;
	start = (((sq_l[1] / 10) + (sq_l[1] / 5) + (sq_l[1] / 3)) * e->mlx->TLN)
			+ ((sq_l[0] / 8) * 3) + (i * sq_l[1] * e->mlx->TLN) + (j * sq_l[0]);
	sq_sz[0] = (e->screen_width / e->mszy) / 15;
	sq_sz[1] = (e->screen_height / e->mszx) / 5;
	i = -1;
	while (i++, sq_sz[1] - (sq_sz[1] / 2) + i < sq_sz[1] + (sq_sz[1] / 2))
	{
		j = -1;
		while (j++, sq_sz[0] - (sq_sz[0] / 2) + j < sq_sz[0] + (sq_sz[0] / 2))
		{
			index = start + (i * e->mlx->total_line_size) + j;
			ft_memcpy(e->mlx->draw + (index * 4), &color,
				e->mlx->color_byte_size);
		}
	}
}

void	draw_lf_legs(t_env *e, int i, int j, int color)
{
	int		start;
	int		sq_sz[2];
	int		sq_l[2];
	int		index;
	float	k;

	sq_l[0] = e->screen_width / e->mszy;
	sq_l[1] = e->screen_height / e->mszx;
	start = (((sq_l[1] / 10) + (sq_l[1] / 5) + (sq_l[1] / 3)) * e->mlx->TLN)
			+ ((sq_l[0] / 8) * 3) + (i * sq_l[1] * e->mlx->TLN) + (j * sq_l[0]);
	sq_sz[0] = (e->screen_width / e->mszy) / 15;
	sq_sz[1] = (e->screen_height / e->mszx) / 5;
	i = -1;
	k = 0;
	while (i++, sq_sz[1] - (sq_sz[1] / 2) + i < sq_sz[1] + (sq_sz[1] / 2))
	{
		j = -1;
		while (j++, sq_sz[0] - (sq_sz[0] / 2) + j < sq_sz[0] + (sq_sz[0] / 2))
		{
			index = start + (i * e->mlx->total_line_size) + j - (int)k;
			ft_memcpy(e->mlx->draw + (index * 4), &color,
				e->mlx->color_byte_size);
		}
		k += (float)sq_sz[0] / (float)sq_sz[1] + 1;
	}
}

void	draw_rs_legs(t_env *e, int i, int j, int color)
{
	int		start;
	int		sq_sz[2];
	int		sq_l[2];
	int		index;

	sq_l[0] = e->screen_width / e->mszy;
	sq_l[1] = e->screen_height / e->mszx;
	start = (((sq_l[1] / 10) + (sq_l[1] / 5) + (sq_l[1] / 3)) * e->mlx->TLN)
			+ ((sq_l[0] / 8) * 4) + (i * sq_l[1] * e->mlx->TLN) + (j * sq_l[0]);
	sq_sz[0] = (e->screen_width / e->mszy) / 15;
	sq_sz[1] = (e->screen_height / e->mszx) / 5;
	i = -1;
	while (i++, sq_sz[1] - (sq_sz[1] / 2) + i < sq_sz[1] + (sq_sz[1] / 2))
	{
		j = -1;
		while (j++, sq_sz[0] - (sq_sz[0] / 2) + j < sq_sz[0] + (sq_sz[0] / 2))
		{
			index = start + (i * e->mlx->total_line_size) + j;
			ft_memcpy(e->mlx->draw + (index * 4), &color,
				e->mlx->color_byte_size);
		}
	}
}

void	draw_rf_legs(t_env *e, int i, int j, int color)
{
	int		start;
	int		sq_sz[2];
	int		sq_l[2];
	int		index;
	float	k;

	sq_l[0] = e->screen_width / e->mszy;
	sq_l[1] = e->screen_height / e->mszx;
	start = (((sq_l[1] / 10) + (sq_l[1] / 5) + (sq_l[1] / 3)) * e->mlx->TLN)
			+ ((sq_l[0] / 8) * 4) + (i * sq_l[1] * e->mlx->TLN) + (j * sq_l[0]);
	sq_sz[0] = (e->screen_width / e->mszy) / 15;
	sq_sz[1] = (e->screen_height / e->mszx) / 5;
	i = -1;
	k = 0;
	while (i++, sq_sz[1] - (sq_sz[1] / 2) + i < sq_sz[1] + (sq_sz[1] / 2))
	{
		j = -1;
		while (j++, sq_sz[0] - (sq_sz[0] / 2) + j < sq_sz[0] + (sq_sz[0] / 2))
		{
			index = start + (i * e->mlx->total_line_size) + j + (int)k;
			ft_memcpy(e->mlx->draw + (index * 4), &color,
				e->mlx->color_byte_size);
		}
		k += (float)sq_sz[0] / (float)sq_sz[1] + 1;
	}
}
