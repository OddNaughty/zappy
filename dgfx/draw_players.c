/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_players.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/12 11:21:51 by rgary             #+#    #+#             */
/*   Updated: 2014/06/12 11:21:51 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

void	draw_head(t_env *e, int i, int j, int color)
{
	int		start;
	int		sq_hsz;
	int		sq_vsz;
	int		sq_l[2];
	int		index;

	sq_l[0] = e->screen_width / e->mszy;
	sq_l[1] = e->screen_height / e->mszx;
	start = ((sq_l[1] / 10) * e->mlx->TLN) + ((sq_l[0] / 8) * 3)
			+ (i * sq_l[1] * e->mlx->TLN) + (j * sq_l[0]);
	sq_hsz = (e->screen_width / e->mszy) / 4;
	sq_vsz = (e->screen_height / e->mszx) / 7;
	i = -1;
	while (i++, sq_vsz - (sq_vsz / 2) + i < sq_vsz + (sq_vsz / 2))
	{
		j = -1;
		while (j++, sq_hsz - (sq_hsz / 2) + j < sq_hsz + (sq_hsz / 2))
		{
			index = start + (i * e->mlx->total_line_size) + j;
			ft_memcpy(e->mlx->draw + (index * 4), &color,
				e->mlx->color_byte_size);
		}
	}
}

void	draw_body(t_env *e, int i, int j, int color)
{
	int		start;
	int		sq_hl;
	int		sq_vl;
	int		sq_hsz;
	int		sq_vsz;
	int		index;

	sq_hl = e->screen_width / e->mszy;
	sq_vl = e->screen_height / e->mszx;
	start = (((sq_vl / 10) + (sq_vl / 7) + (sq_vl / 12)) * e->mlx->TLN)
		+ (sq_hl / 3) + (i * sq_vl * e->mlx->TLN) + (j * sq_hl);
	sq_hsz = (e->screen_width / e->mszy) / 3;
	sq_vsz = (e->screen_height / e->mszx) / 3;
	i = -1;
	while (i++, sq_vsz - (sq_vsz / 2) + i < sq_vsz + (sq_vsz / 2))
	{
		j = -1;
		while (j++, sq_hsz - (sq_hsz / 2) + j < sq_hsz + (sq_hsz / 2))
		{
			index = start + (i * e->mlx->total_line_size) + j;
			ft_memcpy(e->mlx->draw + (index * 4), &color,
				e->mlx->color_byte_size);
		}
	}
}

void	draw_left_neck(t_env *e, int i, int j, int color)
{
	int		start;
	int		sq_hsz;
	int		sq_vsz;
	int		sq_l[2];
	int		index;

	sq_l[0] = e->screen_width / e->mszy;
	sq_l[1] = e->screen_height / e->mszx;
	start = ((sq_l[1] / 5) * e->mlx->TLN) + ((sq_l[0] / 10) * 4)
			+ (i * sq_l[1] * e->mlx->TLN) + (j * sq_l[0]);
	sq_hsz = (e->screen_width / e->mszy) / 10;
	sq_vsz = (e->screen_height / e->mszx) / 5;
	i = -1;
	while (i++, sq_vsz - (sq_vsz / 2) + i < sq_vsz + (sq_vsz / 2))
	{
		j = -1;
		while (j++, sq_hsz - (sq_hsz / 2) + j < sq_hsz + (sq_hsz / 2))
		{
			index = start + (i * e->mlx->total_line_size) + j;
			ft_memcpy(e->mlx->draw + (index * 4), &color,
				e->mlx->color_byte_size);
		}
	}
}

void	draw_right_neck(t_env *e, int i, int j, int color)
{
	int		start;
	int		sq_hsz;
	int		sq_vsz;
	int		sq_l[2];
	int		index;

	sq_l[0] = e->screen_width / e->mszy;
	sq_l[1] = e->screen_height / e->mszx;
	start = ((sq_l[1] / 5) * e->mlx->TLN) + ((sq_l[0] / 10) * 5)
			+ (i * sq_l[1] * e->mlx->TLN) + (j * sq_l[0]);
	sq_hsz = (e->screen_width / e->mszy) / 10;
	sq_vsz = (e->screen_height / e->mszx) / 5;
	i = -1;
	while (i++, sq_vsz - (sq_vsz / 2) + i < sq_vsz + (sq_vsz / 2))
	{
		j = -1;
		while (j++, sq_hsz - (sq_hsz / 2) + j < sq_hsz + (sq_hsz / 2))
		{
			index = start + (i * e->mlx->total_line_size) + j;
			ft_memcpy(e->mlx->draw + (index * 4), &color,
				e->mlx->color_byte_size);
		}
	}
}

void	draw_players(t_env *e)
{
	t_clist			*save;
	t_timeval		timeval;

	gettimeofday(&timeval, NULL);
	save = e->clist;
	while (save)
	{
		if (save->client_nb == 0)
		{
			save = save->next;
			continue ;
		}
		if (save->color == 0)
			save->color = generate_color();
		draw_head(e, save->posx, save->posy, save->color);
		draw_body(e, save->posx, save->posy, save->color);
		if (timeval.tv_usec / 100000 > 4)
			draw_player_one(e, save);
		else
			draw_player_two(e, save);
		save = save->next;
	}
}
