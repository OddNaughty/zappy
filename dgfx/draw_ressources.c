/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_ressources.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/14 10:07:59 by rgary             #+#    #+#             */
/*   Updated: 2014/06/14 10:07:59 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

void	draw_ressources(t_env *e)
{
	int	i;
	int	j;

	i = -1;
	while (i++, i < e->mszx)
	{
		j = -1;
		while (j++, j < e->mszy)
			draw_ress_squares(e, i, j);
	}
}

void	draw_ress_squares(t_env *e, int i, int j)
{
	int	start;
	int	c;
	int	sq_hl;
	int	sq_vl;

	sq_hl = e->screen_width / e->mszy;
	sq_vl = e->screen_height / e->mszx;
	c = -1;
	while (c++, c < NB_RESS)
	{
		if (e->res[i][j][c] > 0)
		{
			start = find_start(e, (i * sq_vl * e->mlx->TLN) + (j * sq_hl), c);
			draw_img(e, start, c);
		}
	}
}

int		find_start(t_env *e, int start, int ress)
{
	int	mod;

	if (ress == 0)
		mod = (((e->screen_height / e->mszx) / 9) * e->mlx->TLN) +
			(((e->screen_width / e->mszy) / 5));
	else if (ress == 1)
		mod = (((e->screen_height / e->mszx) / 9) * e->mlx->TLN) +
			(((e->screen_width / e->mszy) / 5)) * 3;
	else if (ress == 2)
		mod = ((((e->screen_height / e->mszx) / 9) * 3) * e->mlx->TLN) +
			(((e->screen_width / e->mszy) / 5));
	else if (ress == 3)
		mod = ((((e->screen_height / e->mszx) / 9) * 3) * e->mlx->TLN) +
			(((e->screen_width / e->mszy) / 5)) * 3;
	else if (ress == 4)
		mod = ((((e->screen_height / e->mszx) / 9) * 5) * e->mlx->TLN) +
			(((e->screen_width / e->mszy) / 5));
	else if (ress == 5)
		mod = ((((e->screen_height / e->mszx) / 9) * 5) * e->mlx->TLN) +
			(((e->screen_width / e->mszy) / 5)) * 3;
	else
		mod = ((((e->screen_height / e->mszx) / 9) * 7) * e->mlx->TLN) +
			(((e->screen_width / e->mszy) / 5));
	return (mod + start);
}

void	draw_img(t_env *e, int start, int c)
{
	int		i[3];
	float	r[2];
	int		sq[2];
	int		img;

	sq[0] = (e->screen_width / e->mszy) / 5;
	sq[1] = (e->screen_height / e->mszx) / 5;
	r[0] = (float)e->mlx->xpm_w[c] / (float)sq[0];
	r[1] = (float)e->mlx->xpm_h[c] / (float)sq[1];
	i[0] = -1;
	while (i[0]++, (sq[1] - (sq[1] / 2) + i[0]) < (sq[1] + (sq[1] / 2)))
	{
		i[1] = -1;
		while (i[1]++, (sq[0] - (sq[0] / 2) + i[1]) < (sq[0] + (sq[0] / 2)))
		{
			i[2] = start + (i[0] * e->mlx->total_line_size) + i[1];
			ft_memcpy(&img, e->mlx->xpm[c] + (((int)((float)i[0] * r[1]) *
						e->mlx->xpm_w[c]) + (int)((float)i[1] * r[0])) * 4, 4);
			if (img != 0)
				ft_memcpy(e->mlx->draw + (i[2] * 4), &img, e->mlx->CBS);
		}
	}
}
