/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gfx.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/09 11:36:44 by rgary             #+#    #+#             */
/*   Updated: 2014/06/09 11:36:44 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

int		socket_and_connect(t_addr *info)
{
	int		socketfd;

	socketfd = 0;
	while (info)
	{
		if ((socketfd = socket(info->ai_family, info->ai_socktype
			, info->ai_protocol)) == -1)
		{
			info = info->ai_next;
			continue ;
		}
		if (connect(socketfd, info->ai_addr, info->ai_addrlen) == -1)
		{
			info = info->ai_next;
			continue ;
		}
		break ;
	}
	if (info == NULL)
		ft_error_exit("Client failed to connect.\n", 3);
	return (socketfd);
}

int		treat_options(int argc)
{
	if (argc != 3)
		return (0);
	return (1);
}

void	ft_set_data(t_addr *data)
{
	ft_memset(data, 0, sizeof(t_addr));
	data->ai_family = AF_UNSPEC;
	data->ai_socktype = SOCK_STREAM;
}

int		main(int argc, char **argv)
{
	int			sfd;
	t_addr		data;
	t_addr		*info;

	if (treat_options(argc) == 0)
		return (fprintf(stderr, "Usage: ./gfx serveraddress port\n"), 1);
	ft_set_data(&data);
	if (getaddrinfo(argv[1], argv[2], &data, &info) != 0)
		ft_error_exit("Getaddrinfo error. Check the arguments.\n", 1);
	sfd = socket_and_connect(info);
	main_gfx_loop(sfd);
	return (0);
}
