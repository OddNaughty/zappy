/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_imgs.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/13 05:22:52 by rgary             #+#    #+#             */
/*   Updated: 2014/06/13 05:22:52 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

int	insert_img_5(t_env *e)
{
	if (!(MLX->pic[ENDGAME] = MXFTI(MLX->mlx, ENDGAMEIMG, &MLX->xpm_w[ENDGAME],
		&MLX->xpm_h[ENDGAME])))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->xpm[ENDGAME] = MGDA(MLX->pic[ENDGAME], &MLX->bpp,
		&MLX->SILI, &MLX->endian)))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->pic[BDCT] = MXFTI(MLX->mlx, BDCTIMG, &MLX->xpm_w[BDCT],
		&MLX->xpm_h[BDCT])))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->xpm[BDCT] = MGDA(MLX->pic[BDCT], &MLX->bpp,
		&MLX->SILI, &MLX->endian)))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	return (0);
}

int	insert_img_4(t_env *e)
{
	if (!(MLX->pic[BLOOD] = MXFTI(MLX->mlx, BLD, &MLX->xpm_w[BLOOD],
		&MLX->xpm_h[BLOOD])))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->xpm[BLOOD] = MGDA(MLX->pic[BLOOD], &MLX->bpp,
		&MLX->SILI, &MLX->endian)))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->pic[EGG] = MXFTI(MLX->mlx, EGGIMG, &MLX->xpm_w[EGG],
		&MLX->xpm_h[EGG])))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->xpm[EGG] = MGDA(MLX->pic[EGG], &MLX->bpp,
		&MLX->SILI, &MLX->endian)))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->pic[BEGG] = MXFTI(MLX->mlx, BROEGG, &MLX->xpm_w[BEGG],
		&MLX->xpm_h[BEGG])))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->xpm[BEGG] = MGDA(MLX->pic[BEGG], &MLX->bpp,
		&MLX->SILI, &MLX->endian)))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->pic[DEGG] = MXFTI(MLX->mlx, DEAEGG, &MLX->xpm_w[DEGG],
		&MLX->xpm_h[DEGG])))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->xpm[DEGG] = MGDA(MLX->pic[DEGG], &MLX->bpp,
		&MLX->SILI, &MLX->endian)))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	return (insert_img_5(e));
}

int	insert_img_3(t_env *e)
{
	if (!(MLX->xpm[THYSTAME] = MGDA(MLX->pic[THYSTAME], &MLX->bpp, &MLX->SILI,
		&MLX->endian)))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->pic[INCANT] = MXFTI(MLX->mlx, INC, &MLX->xpm_w[INCANT],
		&MLX->xpm_h[INCANT])))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->xpm[INCANT] = MGDA(MLX->pic[INCANT], &MLX->bpp, &MLX->SILI,
		&MLX->endian)))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->pic[INCANTSUCC] = MXFTI(MLX->mlx, INCS, &MLX->xpm_w[INCANTSUCC],
		&MLX->xpm_h[INCANTSUCC])))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->xpm[INCANTSUCC] = MGDA(MLX->pic[INCANTSUCC], &MLX->bpp,
		&MLX->SILI, &MLX->endian)))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->pic[INCANTFAIL] = MXFTI(MLX->mlx, INCF, &MLX->xpm_w[INCANTFAIL],
		&MLX->xpm_h[INCANTFAIL])))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->xpm[INCANTFAIL] = MGDA(MLX->pic[INCANTFAIL], &MLX->bpp,
		&MLX->SILI, &MLX->endian)))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	return (insert_img_4(e));
}

int	insert_img_2(t_env *e)
{
	if (!(MLX->pic[SIBUR] = MXFTI(MLX->mlx, OCB, &MLX->xpm_w[SIBUR],
		&MLX->xpm_h[SIBUR])))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->xpm[SIBUR] = MGDA(MLX->pic[SIBUR], &MLX->bpp, &MLX->SILI,
		&MLX->endian)))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->pic[MENDIANE] = MXFTI(MLX->mlx, RCB, &MLX->xpm_w[MENDIANE],
		&MLX->xpm_h[MENDIANE])))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->xpm[MENDIANE] = MGDA(MLX->pic[MENDIANE], &MLX->bpp, &MLX->SILI,
		&MLX->endian)))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->pic[PHIRAS] = MXFTI(MLX->mlx, VCB, &MLX->xpm_w[PHIRAS],
		&MLX->xpm_h[PHIRAS])))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->xpm[PHIRAS] = MGDA(MLX->pic[PHIRAS], &MLX->bpp, &MLX->SILI,
		&MLX->endian)))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->pic[THYSTAME] = MXFTI(MLX->mlx, CCB, &MLX->xpm_w[THYSTAME],
		&MLX->xpm_h[THYSTAME])))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	return (insert_img_3(e));
}

int	insert_img(t_env *e)
{
	e->mlx->pic = (void**)malloc(sizeof(void*) * TOTALIMG);
	e->mlx->xpm = (char**)malloc(sizeof(char*) * TOTALIMG);
	e->mlx->xpm_w = (int*)malloc(sizeof(int) * TOTALIMG);
	e->mlx->xpm_h = (int*)malloc(sizeof(int) * TOTALIMG);
	if (!(MLX->pic[FOOD] = MXFTI(MLX->mlx, FDIMG, &MLX->xpm_w[FOOD],
		&MLX->xpm_h[FOOD])))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->xpm[FOOD] = MGDA(MLX->pic[FOOD], &MLX->bpp, &MLX->SILI,
		&MLX->endian)))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->pic[LINEMATE] = MXFTI(MLX->mlx, GCB, &MLX->xpm_w[LINEMATE],
		&MLX->xpm_h[LINEMATE])))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->xpm[LINEMATE] = MGDA(MLX->pic[LINEMATE], &MLX->bpp, &MLX->SILI,
		&MLX->endian)))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->pic[DERAUMERE] = MXFTI(MLX->mlx, GRCB, &MLX->xpm_w[DERAUMERE],
		&MLX->xpm_h[DERAUMERE])))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if (!(MLX->xpm[DERAUMERE] = MGDA(MLX->pic[DERAUMERE], &MLX->bpp, &MLX->SILI,
		&MLX->endian)))
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	return (insert_img_2(e));
}
