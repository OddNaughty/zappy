/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_mlx_build.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/09 04:42:53 by rgary             #+#    #+#             */
/*   Updated: 2014/06/09 04:42:53 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

int		expose_hook(t_env *e)
{
	draw_it(e);
	return (0);
}

int		loop_hook(t_env *e)
{
	char			*line;
	struct timeval	s_timeval;

	FD_ZERO(&e->fd_read);
	FD_SET(e->sfd, &e->fd_read);
	s_timeval.tv_sec = 0;
	s_timeval.tv_usec = (int)(((float)1 / (float)ETU) * (float)1000000);
	if (e->move.move_up && e->img_posy < 0)
		e->img_posy += 10;
	if (e->move.move_down && e->img_posy > SCREEN_WIDTH - e->screen_width)
		e->img_posy -= 10;
	if (e->move.move_left && e->img_posx < 0)
		e->img_posx += 10;
	if (e->move.move_right && e->img_posx > SCREEN_HEIGHT - e->screen_height)
		e->img_posx -= 10;
	e->rdy = select(e->sfd + 1, &e->fd_read, &e->fd_write, NULL, &s_timeval);
	if (e->rdy && FD_ISSET(e->sfd, &e->fd_read))
	{
		if ((line = g_recv(e, e->sfd)) != NULL)
			treat_recv_infos(e, line);
		draw_infos(e);
	}
	draw_it(e);
	return (0);
}

int		key_press_event(int keycode, t_env *e)
{
	if (keycode == KEY_UP)
		e->move.move_up = 1;
	if (keycode == KEY_DOWN)
		e->move.move_down = 1;
	if (keycode == KEY_LEFT)
		e->move.move_left = 1;
	if (keycode == KEY_RIGHT)
		e->move.move_right = 1;
	if (keycode == KEY_ESC)
		exit (4);
	if (keycode == KEY_PLUS && e->zoom <= 2.9)
		zoom(e, 1);
	if (keycode == KEY_MINUS && e->zoom > 1.0)
		zoom(e, -1);
	return (0);
}

int		key_release_event(int keycode, t_env *e)
{
	if (keycode == KEY_UP)
		e->move.move_up = 0;
	if (keycode == KEY_DOWN)
		e->move.move_down = 0;
	if (keycode == KEY_LEFT)
		e->move.move_left = 0;
	if (keycode == KEY_RIGHT)
		e->move.move_right = 0;
	return (0);
}

int		create_mlx(t_env *e)
{
	e->mlx = (t_mlx*)malloc(sizeof(t_mlx));
	if ((e->mlx->mlx = mlx_init()) == NULL)
		return (fprintf(stderr, "MLX INIT ERROR. Aborting.\n"), -1);
	if ((e->mlx->win = mlx_new_window(MLX->mlx, SCREEN_WIDTH, SCREEN_HEIGHT,
		"Zappy")) == NULL)
		return (fprintf(stderr, "WIN INIT ERROR. Aborting.\n"), -1);
	if ((e->mlx->win2 = mlx_new_window(MLX->mlx, INFO_WIDTH, INFO_HEIGHT,
		"Zappy")) == NULL)
		return (fprintf(stderr, "WIN INIT ERROR. Aborting.\n"), -1);
	if (insert_img(e) == -1)
		return (-1);
	if ((e->mlx->fimg = mlx_new_image(e->mlx->mlx, INFO_WIDTH,
		INFO_HEIGHT)) == NULL)
		return (fprintf(stderr, "IMG INIT ERROR. Aborting.\n"), -1);
	if ((e->mlx->fdraw = mlx_get_data_addr(e->mlx->fimg, &e->mlx->bpp,
		&e->mlx->size_line, &e->mlx->endian)) == NULL)
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	if ((e->mlx->img = mlx_new_image(e->mlx->mlx, e->screen_width,
		e->screen_height)) == NULL)
		return (fprintf(stderr, "IMG INIT ERROR. Aborting.\n"), -1);
	if ((e->mlx->draw = mlx_get_data_addr(e->mlx->img, &e->mlx->bpp,
		&e->mlx->size_line, &e->mlx->endian)) == NULL)
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	call_to_loop(e);
	return (0);
}
