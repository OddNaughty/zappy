/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   launch_init.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/09 01:21:49 by rgary             #+#    #+#             */
/*   Updated: 2014/06/09 01:21:49 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

t_com	*fill_com_func_2(t_com *f)
{
	f[10] = treat_pie;
	f[11] = treat_pfk;
	f[12] = treat_pdr;
	f[13] = treat_pgt;
	f[14] = treat_pdi;
	f[15] = treat_enw;
	f[16] = treat_eht;
	f[17] = treat_ebo;
	f[18] = treat_edi;
	f[19] = treat_sgt;
	f[20] = treat_seg;
	f[21] = treat_smg;
	f[22] = treat_suc;
	f[23] = treat_sbp;
	return (f);
}

t_com	*fill_com_func(int nb)
{
	static t_com	*f = NULL;

	if (f == NULL)
	{
		if ((f = (t_com*)malloc(sizeof(t_com) * nb)) == NULL)
			ft_error_exit("Malloc error in fill_com_func. Abort.\n", 2);
		f[0] = treat_msz;
		f[1] = treat_bct;
		f[2] = treat_tna;
		f[3] = treat_pnw;
		f[4] = treat_ppo;
		f[5] = treat_plv;
		f[6] = treat_pin;
		f[7] = treat_pex;
		f[8] = treat_pbc;
		f[9] = treat_pic;
		f = fill_com_func_2(f);
	}
	return (f);
}

void	treat_cmd(t_env *e, char *cmd)
{
	t_com		*f;
	int			i;
	static char	*cmds[25] =

	{"msz ", "bct ", "tna ", "pnw ", "ppo ", "plv ", "pin ", "pex ", "pbc ",
	"pic ", "pie ", "pfk ", "pdr ", "pgt ", "pdi ", "enw ", "eht ", "ebo ",
	"edi ", "sgt ", "seg ", "smg ", "suc\0", "sbp\0", NULL};
	i = -1;
	f = fill_com_func(24);
	while (i++, i < 24)
	{
		if (ft_strncmp(cmds[i], cmd, 4) == 0)
		{
			(f[i])(e, cmd);
			return ;
		}
	}
}

void	treat_recv_infos(t_env *e, char *str)
{
	char	**args;
	int		i;

	i = -1;
	args = ft_strsplit(str, '\n');
	while (i++, args[i])
		treat_cmd(e, args[i]);
	free_args(args);
}

void	first_init(t_env *e, int sfd)
{
	char	*str;

	e->res = NULL;
	e->clist = NULL;
	if ((str = g_recv(e, sfd)) == NULL)
	{
		fprintf(stderr, "Error in receiving during first initiatization. ");
		fprintf(stderr, "This client might not work properly.\n");
		exit (5);
	}
	if (g_send(e, sfd, "GRAPHIC\n") == EX_ERROR)
	{
		fprintf(stderr, "Error in sending during first initialization. ");
		fprintf(stderr, "This client might not work properly.\n");
		exit (6);
	}
	if ((str = g_recv(e, sfd)) == NULL)
	{
		fprintf(stderr, "Error in receiving during first initiatization. ");
		fprintf(stderr, "This client might not work properly.\n");
		exit (7);
	}
	treat_recv_infos(e, str);
}
