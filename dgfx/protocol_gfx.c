/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   protocol_gfx.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/09 01:33:53 by rgary             #+#    #+#             */
/*   Updated: 2014/06/09 01:33:53 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

int		g_send(t_env *e, int cs, char *s)
{
	(void)e;
	if (send(cs, s, ft_strlen(s), 0) < 0)
		return (EX_ERROR);
	return (EX_SUCCESS);
}

char	*g_recv(t_env *e, int cs)
{
	int		ret;
	char	*s;
	char	buf[BUF_SIZE + 1];

	(void)e;
	s = NULL;
	while ((ret = recv(cs, buf, BUF_SIZE, 0)) == BUF_SIZE)
	{
		buf[ret] = 0;
		if (s == NULL)
			s = ft_strdup(buf);
		else
			s = ft_strfreejoin(&s, buf);
	}
	buf[ret] = 0;
	if (s == NULL && ret != 0)
		s = ft_strdup(buf);
	else if (ret != 0)
		s = ft_strfreejoin(&s, buf);
	return (s);
}
