/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/16 06:29:57 by rgary             #+#    #+#             */
/*   Updated: 2014/06/16 06:29:57 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

int		generate_color(void)
{
	int		randval;

	randval = rand();
	randval &= 0x00ffffff;
	return (randval);
}

int		zoom(t_env *e, int way)
{
	if (way == 1)
		e->zoom += 0.1;
	else
		e->zoom -= 0.1;
	e->screen_width = (int)((float)SCREEN_WIDTH * e->zoom);
	e->screen_height = (int)((float)SCREEN_HEIGHT * e->zoom);
	mlx_destroy_image(e->mlx->mlx, e->mlx->img);
	if ((e->mlx->img = MNI(e->mlx->mlx, e->screen_width,
		e->screen_height)) == NULL)
		return (fprintf(stderr, "IMG INIT ERROR. Aborting.\n"), -1);
	if ((e->mlx->draw = MGDA(e->mlx->img, &e->mlx->bpp,
		&e->mlx->size_line, &e->mlx->endian)) == NULL)
		return (fprintf(stderr, "DATA ADDR ERROR. Aborting.\n"), -1);
	e->mlx->TLN = e->mlx->size_line / e->mlx->CBS;
	draw_it(e);
	return (0);
}

void	call_to_loop(t_env *e)
{
	e->mlx->color_byte_size = e->mlx->bpp / 8;
	e->mlx->total_line_size = e->mlx->size_line / e->mlx->color_byte_size;
	mlx_hook(e->mlx->win, 2, 1, &key_press_event, e);
	mlx_hook(e->mlx->win2, 2, 1, &key_press_event, e);
	mlx_hook(e->mlx->win, 3, 2, &key_release_event, e);
	mlx_hook(e->mlx->win2, 3, 2, &key_release_event, e);
	mlx_expose_hook(e->mlx->win, &expose_hook, e);
	mlx_loop_hook(e->mlx->mlx, &loop_hook, e);
	mlx_loop(e->mlx->mlx);
}

void	draw_player_one(t_env *e, t_clist *save)
{
	draw_left_neck(e, save->posx, save->posy, save->color);
	draw_r_left_arm(e, save->posx, save->posy, save->color);
	draw_d_right_arm(e, save->posx, save->posy, save->color);
	draw_lf_legs(e, save->posx, save->posy, save->color);
	draw_rs_legs(e, save->posx, save->posy, save->color);
}

void	draw_player_two(t_env *e, t_clist *save)
{
	draw_right_neck(e, save->posx, save->posy, save->color);
	draw_d_left_arm(e, save->posx, save->posy, save->color);
	draw_r_right_arm(e, save->posx, save->posy, save->color);
	draw_ls_legs(e, save->posx, save->posy, save->color);
	draw_rf_legs(e, save->posx, save->posy, save->color);
}
