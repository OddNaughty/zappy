/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   treat_pdi.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/13 10:46:41 by rgary             #+#    #+#             */
/*   Updated: 2014/06/13 10:46:41 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

static void	update_dead(t_env *e, t_clist *save)
{
	e->dead[0] = save->posx;
	e->dead[1] = save->posy;
	e->dead[2] = time(NULL) + ANIMTIME;
}

void		treat_pdi(t_env *e, char *cmd)
{
	char	**args;
	t_clist	*save;

	args = ft_strsplit(cmd, ' ');
	save = e->clist;
	while (save)
	{
		if (ft_atoi(args[1]) == save->client_nb)
		{
			update_dead(e, save);
			if (save->prev)
				save->prev->next = save->next;
			else
				e->clist = save->next;
			if (save->next)
				save->next->prev = save->prev;
			free(save->teamname);
			free(save);
			save = NULL;
		}
		else
			save = save->next;
	}
	free_args(args);
}
