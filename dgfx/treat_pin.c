/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   treat_pin.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/09 03:30:16 by rgary             #+#    #+#             */
/*   Updated: 2014/06/09 03:30:16 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

void	replace_inv(t_clist *user, char **args, t_env *e)
{
	int	i;

	i = -1;
	if ((user->posx = 0), ft_atoi(args[3]) < e->mszx)
		user->posx = ft_atoi(args[3]);
	if ((user->posy = 0), ft_atoi(args[2]) < e->mszy)
		user->posy = ft_atoi(args[2]);
	while (i++, i < NB_RESS)
		user->inv[i] = ft_atoi(args[i + 4]);
}

void	treat_pin(t_env *e, char *cmd)
{
	char	**args;
	t_clist	*save;

	args = ft_strsplit(cmd, ' ');
	save = e->clist;
	if (save)
	{
		while (save)
		{
			if (save->client_nb == ft_atoi(args[1]))
			{
				replace_inv(save, args, e);
				break ;
			}
			save = save->next;
		}
	}
	free_args(args);
}
