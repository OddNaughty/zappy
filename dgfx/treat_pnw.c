/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   treat_pnw.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/09 03:10:00 by rgary             #+#    #+#             */
/*   Updated: 2014/06/09 03:10:00 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

t_clist	*insert_info_in_client_list(t_env *e, char **args)
{
	t_clist		*save;
	t_clist		*new;

	new = (t_clist*)ft_strnew(sizeof(t_clist));
	new->client_nb = ft_atoi(args[1]);
	new->egg_nb = 0;
	new->color = 0;
	if ((new->posx = 0), ft_atoi(args[3]) < e->mszx)
		new->posx = ft_atoi(args[3]);
	if ((new->posy = 0), ft_atoi(args[2]) < e->mszy)
		new->posy = ft_atoi(args[2]);
	new->ori = ft_atoi(args[4]);
	new->lvl = ft_atoi(args[5]);
	new->teamname = ft_strdup(args[6]);
	if (e->clist == NULL)
		return (new);
	save = e->clist;
	while (save->next)
		save = save->next;
	new->prev = save;
	save->next = new;
	return (e->clist);
}

void	treat_pnw(t_env *e, char *cmd)
{
	char	**args;

	args = ft_strsplit(cmd, ' ');
	e->clist = insert_info_in_client_list(e, args);
	free_args(args);
}
