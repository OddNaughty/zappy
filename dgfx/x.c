/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   x.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/09 12:10:33 by rgary             #+#    #+#             */
/*   Updated: 2014/06/09 12:10:33 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gfx.h"

int		x_int(int err, int res, char *str, int line)
{
	if (res == err)
	{
		fprintf(stderr, "%s error (line: %d): %s\n",
				str, line, strerror(errno));
		exit (1);
	}
	return (res);
}

void	*x_void(void *err, void *res, char *str, int line)
{
	if (res == err)
	{
		fprintf(stderr, "%s error (line: %d): %s\n",
				str, line, strerror(errno));
		exit (1);
	}
	return (res);
}
