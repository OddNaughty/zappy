/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   broadfunc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/11 07:55:21 by cwagner           #+#    #+#             */
/*   Updated: 2014/06/24 14:36:37 by mgaspail         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static int	get_where(t_fd *other, int dx, int dy)
{
	static int	from[4][8] =

	{{1, 2, 3, 4, 5, 6, 7, 8}, {3, 4, 5, 6, 7, 8, 1, 2},
	{5, 6, 7, 8, 1, 2, 3, 4}, {7, 8, 1, 2, 3, 4, 5, 6}};
	if (dy > 0)
	{
		if (dy > abs(dx))
			return (from[other->o][0]);
		if (dx == dy)
			return (from[other->o][1]);
		if (-dx == dy)
			return (from[other->o][7]);
	}
	else
	{
		if (abs(dy) > abs(dx))
			return (from[other->o][4]);
		if (dx == -dy)
			return (from[other->o][3]);
		if (dx == dy)
			return (from[other->o][5]);
	}
	if (dx < 0)
		return (from[other->o][6]);
	return (from[other->o][2]);
}

static void	send_to_box(t_env *e, t_fd *a, t_fd *other, char *msg)
{
	int		x;
	int		y;
	int		from;
	char	*tosend;

	if (abs(other->y - a->y) > (e->y / 2))
		y = a->y + ((a->y - other->y) / 2);
	else
		y = other->y;
	if (abs(other->x - a->x) > (e->x / 2))
		x = a->x + ((a->x - other->x) / 2);
	else
		x = other->x;
	if ((x - a->x) == 0 && (y - a->y) == 0)
		from = 0;
	else
		from = get_where(other, (x - a->x), (y - a->y));
	asprintf(&tosend, "message %d,%s", from, msg);
	add_to_bufwrite(&(other->buf_write), tosend);
	ft_strdel(&tosend);
}

static void	send_bc(t_env *e, t_fd *a, char *msg)
{
	int		y;
	int		x;
	int		i;
	t_fd	*other;

	y = -1;
	while (++y < e->y)
	{
		x = -1;
		while (++x < e->x)
		{
			if (e->map[y][x]->player)
			{
				i = 0;
				while (i < e->maxfd)
				{
					other = &(e->fds[i]);
					if ((other->type == FD_CLIENT) && (other != a)
						&& (other->x == x) && (other->y == y))
						send_to_box(e, a, other, msg);
					i++;
				}
			}
		}
	}
}

int			broadcast(t_env *e, t_fd *a)
{
	char	**split;

	if ((split = ft_strsplitwhite(a->act[0].action)) == NULL)
		return (FAILURE);
	if ((ft_strcmp(split[0], "broadcast") != SUCCESS) || !split[1])
		return (FAILURE);
	send_bc(e, a, a->act[0].action + sizeof("broadcast"));
	add_to_bufwrite(&(a->buf_write), "ok");
	gfxsend_broad(e->gfx_fd, a->id, split[1]);
	ft_freechartab(&split);
	return (SUCCESS);
}
