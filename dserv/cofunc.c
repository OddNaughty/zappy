/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cofunc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/11 08:10:42 by cwagner           #+#    #+#             */
/*   Updated: 2014/06/11 08:10:42 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

int			connectnbr(t_env *e, t_fd *a)
{
	char	**split;
	char	*send;
	int		nbr;

	if ((split = ft_strsplitwhite(a->act[0].action)) == NULL)
		return (FAILURE);
	if (ft_strcmp(split[0], STR_CON_NB) != SUCCESS)
		return (FAILURE);
	nbr = a->team->nbmax_player - a->team->nb_player;
	send = ft_itoa(nbr);
	ft_freechartab(&split);
	add_to_bufwrite(&(a->buf_write), send);
	ft_strdel(&send);
	(void)e;
	return (SUCCESS);
}
