/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   eggfunc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/11 07:59:37 by cwagner           #+#    #+#             */
/*   Updated: 2014/06/11 07:59:37 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void			set_egg_exectimes(t_env *e, t_egg *egg)
{
	t_tval		t;
	int			carry;
	int			usec;

	gettimeofday(&t, NULL);
	usec = ((USEC_IN_SEC * UT_HATCH) / e->t) % USEC_IN_SEC;
	carry = (t.tv_usec + usec) / USEC_IN_SEC;
	egg->hatch.tv_usec = (t.tv_usec + usec) % USEC_IN_SEC;
	egg->hatch.tv_sec = (((USEC_IN_SEC * UT_HATCH) / e->t) / USEC_IN_SEC)
		+ carry + t.tv_sec;
	usec = ((USEC_IN_SEC * UT_ROT) / e->t) % USEC_IN_SEC;
	carry = (t.tv_usec + usec) / USEC_IN_SEC;
	egg->rot.tv_usec = (egg->hatch.tv_usec + usec) % USEC_IN_SEC;
	egg->rot.tv_sec = (((USEC_IN_SEC * UT_ROT) / e->t) / USEC_IN_SEC)
		+ carry + egg->hatch.tv_sec;
}

static t_egg	*create_egg(t_env *e, t_fd *a)
{
	t_egg		*egg;
	static int	id =

	0;
	srand(rand());
	egg = (t_egg*)XV(NULL, malloc(sizeof(t_egg)), "malloc");
	egg->id = ++id;
	egg->x = a->x;
	egg->y = a->y;
	egg->hatched = 0;
	egg->team = a->team;
	set_egg_exectimes(e, egg);
	egg->next = e->egglist;
	e->egglist = egg;
	return (egg);
}

int				egg(t_env *e, t_fd *a)
{
	char	**split;
	t_egg	*newegg;

	if ((split = ft_strsplitwhite(a->act[0].action)) == NULL)
		return (FAILURE);
	if (ft_strcmp(split[0], "fork") != SUCCESS)
		return (FAILURE);
	ft_freechartab(&split);
	add_to_bufwrite(&(a->buf_write), "ok");
	gfxsend_playerlay(e->gfx_fd, a->id);
	newegg = create_egg(e, a);
	gfxsend_egglay(e->gfx_fd, a->id, newegg);
	return (SUCCESS);
}
