/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expulsefunc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/10 07:02:36 by cwagner           #+#    #+#             */
/*   Updated: 2014/06/10 07:02:36 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static void	manage_expulse(t_env *e, t_fd *a, t_fd *other)
{
	char		*send;
	static int	from[4][4] =

	{{5, 7, 1, 3}, {3, 5, 7, 1}, {1, 3, 5, 7}, {7, 1, 3, 5}};
	e->map[a->y][a->x]->player--;
	if (a->o == NORTH)
		other->y = REALP(a->y - 1, e->y);
	else if (a->o == WEST)
		other->x = REALP(a->x - 1, e->x);
	else if (a->o == EAST)
		other->x = REALP(a->x + 1, e->x);
	else
		other->y = REALP(a->y + 1, e->y);
	e->map[other->y][other->x]->player++;
	asprintf(&send, "deplacement %d", from[a->o][other->o]);
	add_to_bufwrite(&(other->buf_write), send);
	gfxsend_pos(e->gfx_fd, other);
	ft_strdel(&send);
}

int			expulse(t_env *e, t_fd *a)
{
	char	**split;
	int		i;
	t_fd	*other;

	i = 0;
	if ((split = ft_strsplitwhite(a->act[0].action)) == NULL)
		return (FAILURE);
	if (ft_strcmp(split[0], "expulse") != SUCCESS)
		return (FAILURE);
	while (i < e->maxfd)
	{
		other = &(e->fds[i]);
		if ((other->type == FD_CLIENT) && (other != a) &&
			((other->x == a->x) && (other->y == a->y)))
			manage_expulse(e, a, other);
		i++;
	}
	ft_freechartab(&split);
	add_to_bufwrite(&(a->buf_write), "ok");
	gfxsend_expulse(e->gfx_fd, a->id);
	return (SUCCESS);
}
