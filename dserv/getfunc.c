/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getfunc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/10 06:13:16 by cwagner           #+#    #+#             */
/*   Updated: 2014/06/10 06:13:16 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void		fill_translate(char *translate[NB_RESS])
{
	translate[0] = ft_strdup("nourriture");
	translate[1] = ft_strdup("linemate");
	translate[2] = ft_strdup("deraumere");
	translate[3] = ft_strdup("sibur");
	translate[4] = ft_strdup("mendiane");
	translate[5] = ft_strdup("phiras");
	translate[6] = ft_strdup("thystame");
}

int			get(t_env *e, t_fd *a)
{
	char	**split;
	char	*send;
	int		i;
	char	*translate[NB_RESS];

	fill_translate(translate);
	if (((split = ft_strsplitwhite(a->act[0].action)) == NULL)
		|| (ft_strcmp(split[0], STR_TAKE) != SUCCESS) || !split[1])
		return (FAILURE);
	i = 0;
	while ((i < NB_RESS) && ft_strcmp(split[1], translate[i]))
		i++;
	if ((i == NB_RESS) || (e->map[a->y][a->x]->ress[i] == 0))
		send = ft_strdup("ko");
	else
	{
		e->map[a->y][a->x]->ress[i]--;
		a->inventory[i]++;
		send = ft_strdup("ok");
	}
	ft_freechartab(&split);
	add_to_bufwrite(&(a->buf_write), send);
	gfxsend_get(e->gfx_fd, e->map[a->y][a->x], a, i);
	ft_strdel(&send);
	return (SUCCESS);
}
