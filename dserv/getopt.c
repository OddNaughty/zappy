/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getopt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/09 05:08:17 by cwagner           #+#    #+#             */
/*   Updated: 2014/06/09 05:08:17 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

int		getoptnumbers(t_env *e, int opt, char *av)
{
	int		i;

	i = -1;
	while (av[++i])
	{
		if (av[i] < '0' || av[i] > '9')
			return (EX_ERROR);
	}
	i = ft_atoi(av);
	if (i == 0)
		return (EX_ERROR);
	if (opt == OPT_PORT)
		e->port = i;
	else if (opt == OPT_NBCLI)
		e->nbcli = i;
	else
		e->t = i;
	return (EX_SUCCESS);
}

int		getwinsize(t_env *e, int opt, char *av)
{
	int		i;

	i = -1;
	while (av[++i])
	{
		if (av[i] < '0' || av[i] > '9')
			return (EX_ERROR);
	}
	i = ft_atoi(av);
	if (i < MIN_WINSIZE)
		return (EX_ERROR);
	if (opt == OPT_WIDTH)
		e->x = i;
	else
		e->y = i;
	return (EX_SUCCESS);
}

int		getteams(t_env *e, char *av)
{
	t_team		*t;

	t = (t_team*)XV(NULL, malloc(sizeof(t_team)), "malloc");
	t->nb_player = 0;
	t->level8 = 0;
	t->name = av;
	t->next = e->team;
	e->team = t;
	return (EX_SUCCESS);
}

void	set_team_maxplayer(t_env *e)
{
	t_team	*t;

	t = e->team;
	while (t)
	{
		t->nbmax_player = e->nbcli;
		t = t->next;
	}
}

int		get_opt(t_env *e, int ac, char **av)
{
	int		i;
	int		opt;
	int		flag;

	i = 0;
	opt = OPT_NONE;
	while (i++, av[i] && i < ac)
	{
		flag = EX_SUCCESS;
		if (av[i][0] == '-')
			opt = checkopt(av[i]);
		else if (opt == OPT_PORT || opt == OPT_NBCLI || opt == OPT_TIME)
			flag = getoptnumbers(e, opt, av[i]);
		else if (opt == OPT_WIDTH || opt == OPT_HEIGHT)
			flag = getwinsize(e, opt, av[i]);
		else if (opt == OPT_TEAM)
			flag = getteams(e, av[i]);
		if (opt == EX_ERROR || flag == EX_ERROR)
			return (EX_ERROR);
	}
	if (e->team == NULL)
		return (EX_ERROR);
	set_team_maxplayer(e);
	return (EX_SUCCESS);
}
