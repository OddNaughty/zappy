/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getopt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/09 05:08:17 by cwagner           #+#    #+#             */
/*   Updated: 2014/06/09 05:08:17 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

int		checkopt(char *s)
{
	if (!ft_strcmp(STR_PORT, s))
		return (OPT_PORT);
	if (!ft_strcmp(STR_WIDTH, s))
		return (OPT_WIDTH);
	if (!ft_strcmp(STR_HEIGHT, s))
		return (OPT_HEIGHT);
	if (!ft_strcmp(STR_TEAM, s))
		return (OPT_TEAM);
	if (!ft_strcmp(STR_NBCLI, s))
		return (OPT_NBCLI);
	if (!ft_strcmp(STR_TIME, s))
		return (OPT_TIME);
	return (EX_ERROR);
}
