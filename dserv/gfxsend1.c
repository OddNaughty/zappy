/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gfxsend1.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/12 03:48:30 by cwagner           #+#    #+#             */
/*   Updated: 2014/06/12 03:48:30 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void		gfxsend_expulse(t_fd *gfx, int id)
{
	char	*tosend;

	if (gfx == NULL)
		return ;
	asprintf(&tosend, "pex %d", id);
	add_to_bufwrite(&(gfx->buf_write), tosend);
	ft_strdel(&tosend);
}

void		gfxsend_pos(t_fd *gfx, t_fd *a)
{
	char	*tosend;

	if (gfx == NULL)
		return ;
	asprintf(&tosend, "ppo %d %d %d %d", a->id, a->x, a->y, a->o + 1);
	add_to_bufwrite(&(gfx->buf_write), tosend);
	ft_strdel(&tosend);
}

void		gfxsend_ik(t_fd *gfx, t_fd *a, int number, int *ids)
{
	int		i;
	char	*tosend;

	i = 0;
	if (gfx == NULL)
		return ;
	asprintf(&tosend, "pic %d %d %d", a->x, a->y, a->level);
	while (i < number)
	{
		tosend = ft_strfreejoin(&tosend, " ");
		tosend = ft_strfreejoin(&tosend, ft_itoa(ids[i]));
		i++;
	}
	add_to_bufwrite(&(gfx->buf_write), tosend);
	ft_strdel(&tosend);
}

void		gfxsend_inc(t_fd *gfx, t_fd *a, int success)
{
	char	*tosend;

	if (gfx == NULL)
		return ;
	asprintf(&tosend, "pie %d %d %d", a->inc_x, a->inc_y, success);
	add_to_bufwrite(&(gfx->buf_write), tosend);
	ft_strdel(&tosend);
}

void		gfxsend_lvlup(t_fd *gfx, int id, int level)
{
	char	*tosend;

	if (gfx == NULL)
		return ;
	asprintf(&tosend, "plv %d %d", id, level);
	add_to_bufwrite(&(gfx->buf_write), tosend);
	ft_strdel(&tosend);
}
