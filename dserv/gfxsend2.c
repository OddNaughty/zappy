/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gfxsend2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/12 08:02:56 by cwagner           #+#    #+#             */
/*   Updated: 2014/06/12 08:02:56 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void		gfxsend_content(t_fd *gfx, int x, int y, t_res *box)
{
	char	*tosend;

	if (gfx == NULL)
		return ;
	asprintf(&tosend, "bct %d %d %d %d %d %d %d %d %d", x, y, box->ress[0],
				box->ress[1], box->ress[2], box->ress[3], box->ress[4],
				box->ress[5], box->ress[6]);
	add_to_bufwrite(&(gfx->buf_write), tosend);
	ft_strdel(&tosend);
}

void		gfxsend_inventory(t_fd *gfx, t_fd *a)
{
	char	*tosend;

	if (gfx == NULL)
		return ;
	asprintf(&tosend, "pin %d %d %d %d %d %d %d %d %d %d", a->id, a->x, a->y,
				a->inventory[0], a->inventory[1], a->inventory[2],
				a->inventory[3], a->inventory[4], a->inventory[5],
				a->inventory[6]);
	add_to_bufwrite(&(gfx->buf_write), tosend);
	ft_strdel(&tosend);
}

void		gfxsend_get(t_fd *gfx, t_res *box, t_fd *a, int res)
{
	char	*tosend;

	if (gfx == NULL)
		return ;
	asprintf(&tosend, "pgt %d %d", a->id, res);
	add_to_bufwrite(&(gfx->buf_write), tosend);
	ft_strdel(&tosend);
	gfxsend_inventory(gfx, a);
	gfxsend_content(gfx, a->x, a->y, box);
}

void		gfxsend_newplayer(t_fd *gfx, t_fd *a)
{
	char	*send;

	if (gfx == NULL)
		return ;
	asprintf(&send, "pnw %d %d %d %d %d %s", a->id, a->x, a->y, a->o + 1,
			a->level, a->team->name);
	add_to_bufwrite(&(gfx->buf_write), send);
	ft_strdel(&send);
}

void		gfxsend_put(t_fd *gfx, t_res *box, t_fd *a, int res)
{
	char	*tosend;

	if (gfx == NULL)
		return ;
	asprintf(&tosend, "pdr %d %d", a->id, res);
	add_to_bufwrite(&(gfx->buf_write), tosend);
	ft_strdel(&tosend);
	gfxsend_inventory(gfx, a);
	gfxsend_content(gfx, a->x, a->y, box);
}
