/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gfxsend3.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/17 11:59:51 by cwagner           #+#    #+#             */
/*   Updated: 2014/06/17 11:59:51 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void		gfxsend_broad(t_fd *gfx, int id, char *msg)
{
	char	*tosend;

	if (gfx == NULL)
		return ;
	asprintf(&tosend, "pbc %d %s", id, msg);
	add_to_bufwrite(&(gfx->buf_write), tosend);
	ft_strdel(&tosend);
}

void		gfxsend_death(t_fd *gfx, int id)
{
	char	*tosend;

	if (gfx == NULL)
		return ;
	asprintf(&tosend, "pdi %d", id);
	add_to_bufwrite(&(gfx->buf_write), tosend);
	ft_strdel(&tosend);
}

void		gfxsend_playerlay(t_fd *gfx, int id)
{
	char	*tosend;

	if (gfx == NULL)
		return ;
	asprintf(&tosend, "pfk %d", id);
	add_to_bufwrite(&(gfx->buf_write), tosend);
	ft_strdel(&tosend);
}

void		gfxsend_egglay(t_fd *gfx, int id, t_egg *egg)
{
	char	*tosend;

	if (gfx == NULL)
		return ;
	asprintf(&tosend, "enw %d %d %d %d", egg->id, id, egg->x, egg->y);
	add_to_bufwrite(&(gfx->buf_write), tosend);
	ft_strdel(&tosend);
}
