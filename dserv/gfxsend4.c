/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gfxsend3.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/17 11:59:51 by cwagner           #+#    #+#             */
/*   Updated: 2014/06/17 11:59:51 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void		gfxsend_egghatch(t_fd *gfx, t_egg *egg)
{
	char	*tosend;

	if (gfx == NULL)
		return ;
	asprintf(&tosend, "eht %d", egg->id);
	add_to_bufwrite(&(gfx->buf_write), tosend);
	ft_strdel(&tosend);
}

void		gfxsend_eggrot(t_fd *gfx, t_egg *egg)
{
	char	*tosend;

	if (gfx == NULL)
		return ;
	asprintf(&tosend, "edi %d", egg->id);
	add_to_bufwrite(&(gfx->buf_write), tosend);
	ft_strdel(&tosend);
}

void		gfxsend_victory(t_fd *gfx, char *team_name)
{
	char	*tosend;

	if (gfx == NULL)
		return ;
	asprintf(&tosend, "seg %s", team_name);
	add_to_bufwrite(&(gfx->buf_write), tosend);
	ft_strdel(&tosend);
}
