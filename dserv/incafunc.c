/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   incafunc.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/11 06:11:23 by cwagner           #+#    #+#             */
/*   Updated: 2014/06/11 06:11:23 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static int	check_playerlvls(t_env *e, t_fd *a)
{
	int		i;
	t_fd	*other;
	int		number;
	int		need;

	i = 0;
	number = 0;
	while (i < e->maxfd)
	{
		other = &(e->fds[i]);
		if ((other->type == FD_CLIENT) &&
			((other->x == a->x) && (other->y == a->y) &&
			(other->level == a->level)))
			number++;
		i++;
	}
	if (((a->level % 2) == 0) || (a->level == 1))
		need = a->level;
	else
		need = a->level - 1;
	if (number < need)
		return (FAILURE);
	return (number);
}

static int	check_stones(t_env *e, t_fd *a)
{
	int				i;
	static int		lvlstones[7][7] =

	{{0, 1, 0, 0, 0, 0, 0}, {0, 1, 1, 1, 0, 0, 0}, {0, 2, 0, 1, 0, 2, 0},
	{0, 1, 1, 2, 0, 1, 0}, {0, 1, 2, 1, 3, 0, 0}, {0, 1, 2, 3, 0, 1, 0},
	{0, 2, 2, 2, 2, 2, 1}};
	i = 1;
	while (i < NB_RESS)
	{
		if (e->map[a->y][a->x]->ress[i] < lvlstones[a->level - 1][i])
			return (FAILURE);
		i++;
	}
	return (SUCCESS);
}

static void	do_levelup(t_env *e, t_fd *a)
{
	int		i;
	int		level;
	t_fd	*other;
	char	*send;

	i = 0;
	level = a->level;
	while (i < e->maxfd)
	{
		other = &(e->fds[i]);
		if ((other->type == FD_CLIENT) &&
			((other->x == a->x) && (other->y == a->y) &&
			(other->level == level)))
		{
			other->level++;
			asprintf(&send, "niveau actuel : %d", other->level);
			add_to_bufwrite(&(other->buf_write), send);
			ft_strdel(&send);
			gfxsend_lvlup(e->gfx_fd, other->id, other->level);
			treat_levels(other, e, i);
		}
		i++;
	}
}

int			checksend_incantation(t_env *e, t_fd *a)
{
	int		i;
	t_fd	*other;
	int		*ids;
	int		number;
	int		j;

	if (((number = check_playerlvls(e, a)) == FAILURE) || (check_stones(e, a)
		== FAILURE))
		return (FAILURE);
	i = 0;
	j = 0;
	ids = malloc(sizeof(*ids) * number);
	while (i < e->maxfd)
	{
		other = &(e->fds[i]);
		if ((other->type == FD_CLIENT) &&
			((other->x == a->x) && (other->y == a->y) &&
			(other->level == a->level)))
			j = result_from_if(ids, j, a, other);
		i++;
	}
	gfxsend_ik(e->gfx_fd, a, number, ids);
	return (SUCCESS);
}

int			incantation(t_env *e, t_fd *a)
{
	char	**split;

	if ((split = ft_strsplitwhite(a->act[0].action)) == NULL)
		return (FAILURE);
	if (ft_strcmp(split[0], "incantation") != SUCCESS)
		return (FAILURE);
	if ((check_playerlvls(e, a) == FAILURE) || (check_stones(e, a) == FAILURE))
	{
		gfxsend_inc(e->gfx_fd, a, 1);
		return (FAILURE);
	}
	gfxsend_inc(e->gfx_fd, a, 0);
	do_levelup(e, a);
	ft_freechartab(&split);
	stone_explode(e, e->map[a->y][a->x], (a->level - 1));
	gfxsend_content(e->gfx_fd, a->x, a->y, e->map[a->y][a->x]);
	return (SUCCESS);
}
