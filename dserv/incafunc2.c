/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   incafunc2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/13 03:05:20 by cwagner           #+#    #+#             */
/*   Updated: 2014/06/13 03:05:20 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void		treat_levels(t_fd *other, t_env *e, int i)
{
	if (other->level == 8)
		other->team->level8++;
	if (other->team->level8 == 6)
	{
		printf("%s won the game !!!\n", e->fds[i].team->name);
		gfxsend_victory(e->gfx_fd, e->fds[i].team->name);
		e->gameover = 1;
	}
}

int			result_from_if(int *ids, int j, t_fd *a, t_fd *other)
{
	ids[j] = other->id;
	other->inc_x = a->x;
	other->inc_y = a->y;
	j++;
	add_to_bufwrite(&(other->buf_write), "elevation en cours");
	return (j);
}

void		stone_explode(t_env *e, t_res *box, int level)
{
	int				i;
	static int		lvlstones[NB_LVLUP][NB_RESS] =

	{{0, 1, 0, 0, 0, 0, 0}, {0, 1, 1, 1, 0, 0, 0}, {0, 2, 0, 1, 0, 2, 0},
	{0, 1, 1, 2, 0, 1, 0}, {0, 1, 2, 1, 3, 0, 0}, {0, 1, 2, 3, 0, 1, 0},
	{0, 2, 2, 2, 2, 2, 1}};
	i = 1;
	while (i < NB_RESS)
	{
		box->ress[i] -= lvlstones[level - 1][i];
		add_nress(e, i, lvlstones[level - 1][i]);
		i++;
	}
}
