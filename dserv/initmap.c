/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initmap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sbuono <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/02 14:00:58 by sbuono            #+#    #+#             */
/*   Updated: 2014/06/02 14:01:00 by sbuono           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void			add_ress(t_env *e, int ress)
{
	int		x;
	int		y;

	srand(rand());
	x = rand() % e->x;
	y = rand() % e->y;
	(e->map[y][x]->ress[ress])++;
	gfxsend_content(e->gfx_fd, x, y, e->map[y][x]);
}

void			add_nress(t_env *e, int ress, int n)
{
	while (n--)
		add_ress(e, ress);
}

void			init_ressources(t_env *e)
{
	int		i;
	int		limit;

	i = -1;
	limit = NB_FOOD(e->x, e->y) - NB_CRESS(e->x, e->y);
	limit = (limit > 0 ? NB_FOOD(e->x, e->y) : NB_CRESS(e->x, e->y) + 1);
	while (++i < limit)
	{
		if (i < NB_LRESS(e->x, e->y) + 1)
			add_ress(e, THYSTAME);
		if (i < NB_RRESS(e->x, e->y) + 1)
		{
			add_ress(e, PHIRAS);
			add_ress(e, MENDIANE);
		}
		if (i < NB_CRESS(e->x, e->y) + 1)
		{
			add_ress(e, SIBUR);
			add_ress(e, DERAUMERE);
			add_ress(e, LINEMATE);
		}
		if (i < NB_FOOD(e->x, e->y))
			add_ress(e, FOOD);
	}
}

void			initbox(t_env *e, int y, int x)
{
	t_res	*ret;
	int		j;

	j = 0;
	ret = (t_res*)XV(NULL, malloc(sizeof(t_res)), "malloc");
	ft_bzero(ret, sizeof(*ret));
	e->map[y][x] = ret;
	e->map[y][x]->player = 0;
	while (j++ < NB_RESS)
		e->map[y][x]->ress[j] = 0;
}

void			initmap(t_env *e)
{
	int		x;
	int		y;
	int		i;

	i = 0;
	y = 0;
	e->map = (t_res***)XV(NULL, malloc(sizeof(t_res**) * e->y), "malloc");
	while (y < e->y)
	{
		x = 0;
		e->map[y] = (t_res**)XV(NULL, malloc(sizeof(t_res*) * e->x), "malloc");
		while (x < e->x)
		{
			initbox(e, y, x);
			x++;
			i++;
		}
		y++;
	}
	init_ressources(e);
}
