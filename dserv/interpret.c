/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   interpret.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/09 05:18:36 by cwagner           #+#    #+#             */
/*   Updated: 2014/06/09 05:18:36 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void	interpret(t_env *e, t_fd *a)
{
	int			i;
	static int	(*func[NB_ACT])(t_env *e, t_fd *a) =

	{ move, right, left, see, inventory, get, put, expulse, broadcast,
	incantation, egg, connectnbr };
	i = 0;
	while ((i < NB_ACT) && (func[i](e, a) != SUCCESS))
		i++;
	if (i == NB_ACT)
		add_to_bufwrite(&(a->buf_write), "ko");
}
