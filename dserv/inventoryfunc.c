/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   inventory.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/10 05:11:03 by cwagner           #+#    #+#             */
/*   Updated: 2014/06/10 05:11:03 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

int			inventory(t_env *e, t_fd *a)
{
	char	**split;
	char	*inventory;

	if ((split = ft_strsplitwhite(a->act[0].action)) == NULL)
		return (FAILURE);
	if (ft_strcmp(split[0], "inventaire") != SUCCESS)
		return (FAILURE);
	asprintf(&inventory, PSTR7, a->inventory[0], a->inventory[1],
		a->inventory[2], a->inventory[3], a->inventory[4], a->inventory[5],
		a->inventory[6]);
	ft_freechartab(&split);
	add_to_bufwrite(&a->buf_write, inventory);
	ft_strdel(&inventory);
	(void)e;
	return (SUCCESS);
}
