/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mainloop.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sbuono <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/02 13:51:31 by sbuono            #+#    #+#             */
/*   Updated: 2014/06/02 13:51:32 by sbuono           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void	init_fd(t_env *e)
{
	int	i;

	i = 0;
	e->max = 0;
	FD_ZERO(&e->fd_read);
	FD_ZERO(&e->fd_write);
	while (i < e->maxfd)
	{
		if (e->fds[i].type != FD_FREE)
		{
			FD_SET(i, &e->fd_read);
			if (e->fds[i].buf_write)
				FD_SET(i, &e->fd_write);
			e->max = MAX(e->max, i);
		}
		i++;
	}
}

void	check_fd(t_env *e)
{
	int	i;

	i = 0;
	while ((i < (e->max + 1)) && (e->r > 0))
	{
		if (FD_ISSET(i, &e->fd_read))
		{
			if (&e->fds[i] && e->fds[i].fct_read)
				e->fds[i].fct_read(e, i);
		}
		if (FD_ISSET(i, &e->fd_write))
		{
			if (&e->fds[i] && e->fds[i].fct_write)
				e->fds[i].fct_write(e, i);
		}
		if (FD_ISSET(i, &e->fd_read) || FD_ISSET(i, &e->fd_write))
			e->r--;
		i++;
	}
}

void	set_interval(t_tval *t, t_env *e)
{
	t_tval		cu_time;

	gettimeofday(&cu_time, NULL);
	get_interval(t, &cu_time, &e->next_update);
}

void	update_eggs(t_env *e)
{
	t_egg	**tmp;
	t_egg	*tmp2;
	t_tval	t;

	tmp = &e->egglist;
	gettimeofday(&t, NULL);
	while (*tmp)
	{
		if ((*tmp)->hatched == 0 && tval_cmp(&t, &((*tmp)->hatch)) <= 0)
		{
			(*tmp)->team->nbmax_player++;
			(*tmp)->hatched++;
			gfxsend_egghatch(e->gfx_fd, *tmp);
		}
		if (tval_cmp(&t, &((*tmp)->rot)) <= 0)
		{
			gfxsend_eggrot(e->gfx_fd, *tmp);
			tmp2 = *tmp;
			*tmp = (*tmp)->next;
			free(tmp2);
		}
		else
			tmp = &((*tmp)->next);
	}
}

void	mainloop(t_env *e)
{
	t_tval		t;
	t_tval		*p;
	int			i;

	i = -1;
	while (e->gameover == 0)
	{
		p = NULL;
		e->next_update.tv_sec = 0;
		update(e);
		update_eggs(e);
		init_fd(e);
		if (e->next_update.tv_sec != 0)
		{
			set_interval(&t, e);
			p = &t;
		}
		e->r = select(e->max + 1, &e->fd_read, &e->fd_write, NULL, p);
		check_fd(e);
	}
	while (i++ < e->maxfd && (e->fds[i].type == FD_CLIENT
		|| e->fds[i].type == FD_GFX))
		close(i);
	exit (SUCCESS);
}
