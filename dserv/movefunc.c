/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   servfunc1.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/09 16:01:48 by cwagner           #+#    #+#             */
/*   Updated: 2014/06/09 16:01:50 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

int			move(t_env *e, t_fd *a)
{
	char	**split;

	if ((split = ft_strsplitwhite(a->act[0].action)) == NULL)
		return (FAILURE);
	if (ft_strcmp(split[0], "avance") != SUCCESS)
		return (FAILURE);
	e->map[a->y][a->x]->player--;
	if (a->o == NORTH)
		a->y = REALP((a->y - 1), e->y);
	else if (a->o == WEST)
		a->x = REALP((a->x - 1), e->x);
	else if (a->o == EAST)
		a->x = REALP((a->x + 1), e->x);
	else
		a->y = REALP((a->y + 1), e->y);
	e->map[a->y][a->x]->player++;
	gfxsend_pos(e->gfx_fd, a);
	add_to_bufwrite(&a->buf_write, "ok");
	ft_freechartab(&split);
	return (SUCCESS);
}
