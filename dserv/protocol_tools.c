/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   protocol_tools.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sbuono <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/09 11:50:26 by sbuono            #+#    #+#             */
/*   Updated: 2014/06/09 11:50:26 by sbuono           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"
#include "clse.h"

void	close_client(t_env *e, int cs)
{
	close(cs);
	clean_fd(&e->fds[cs]);
	printf("Client #%d gone away\n", cs);
}

void	send_error(int cs, int errorcode)
{
	t_send		s;

	s.code = errorcode;
	s.nbbytes = 0;
	send(cs, &s, sizeof(s), 0);
}

void	send_ok(int cs)
{
	t_send		s;

	s.code = IS_OK;
	s.nbbytes = 0;
	while (send(cs, &s, sizeof(s), 0) == FAILURE)
		(usleep(10));
}

int		p_send(t_env *e, int cs, char *s)
{
	if (send(cs, s, ft_strlen(s), 0) < 0)
		return (kill_client(e, cs), EX_ERROR);
	return (EX_SUCCESS);
}

char	*p_recv(t_env *e, int cs)
{
	char	*s;
	char	buf[BUF_SIZE + 1];
	int		ret;

	s = NULL;
	while ((ret = recv(cs, buf, BUF_SIZE, 0)) == BUF_SIZE)
	{
		buf[ret] = 0;
		if (s == NULL)
			s = ft_strdup(buf);
		else
			s = ft_strfreejoin(&s, buf);
	}
	if (ret < 1)
		return (kill_client(e, cs), NULL);
	buf[ret] = 0;
	if (s == NULL)
		s = ft_strdup(buf);
	else
		s = ft_strfreejoin(&s, buf);
	return (s);
}
