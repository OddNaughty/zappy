/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putfunc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/10 06:56:38 by cwagner           #+#    #+#             */
/*   Updated: 2014/06/10 06:56:38 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

int			put(t_env *e, t_fd *a)
{
	char	**split;
	char	*send;
	int		i;
	char	*translate[NB_RESS];

	fill_translate(translate);
	if ((split = ft_strsplitwhite(a->act[0].action)) == NULL)
		return (FAILURE);
	if (i = 0, (ft_strcmp(split[0], "pose") != SUCCESS) || !split[1])
		return (FAILURE);
	while ((i < NB_RESS) && ft_strcmp(split[1], translate[i]))
		i++;
	if ((i == NB_RESS) || (a->inventory[i] == 0))
		send = ft_strdup("ko");
	else
	{
		e->map[a->y][a->x]->ress[i]++;
		a->inventory[i]--;
		send = ft_strdup("ok");
	}
	ft_freechartab(&split);
	add_to_bufwrite(&(a->buf_write), send);
	gfxsend_put(e->gfx_fd, e->map[a->y][a->x], a, i);
	ft_strdel(&send);
	return (SUCCESS);
}
