/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sbuono <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/30 17:09:32 by sbuono            #+#    #+#             */
/*   Updated: 2014/05/30 17:09:32 by sbuono           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

int		retrieve_delay(char *a, t_env *e, int cs)
{
	if (!ft_strcmp(STR_ADVANCE, a) || !ft_strcmp(STR_RIGHT, a)
	|| !ft_strcmp(STR_LEFT, a) || !ft_strcmp(STR_SEE, a)
	|| !ft_strncmp(STR_TAKE, a, ft_strlen(STR_TAKE))
	|| !ft_strncmp(STR_DROP, a, ft_strlen(STR_DROP))
	|| !ft_strcmp(STR_EXPULSE, a)
	|| !ft_strncmp(STR_CAST, a, ft_strlen(STR_CAST)))
		return ((USEC_IN_SEC * UT_ADVANCE) / e->t);
	if (!ft_strcmp(STR_INVENTORY, a))
		return ((USEC_IN_SEC * UT_INVENTORY) / e->t);
	if (!ft_strcmp(STR_SPELL, a) && (checksend_incantation(e, &e->fds[cs])
		== SUCCESS))
		return ((USEC_IN_SEC * UT_SPELL) / e->t);
	if (!ft_strcmp(STR_FORK, a))
		return ((USEC_IN_SEC * UT_FORK) / e->t);
	if (!ft_strcmp(STR_CON_NB, a))
		return ((USEC_IN_SEC * UT_CON_NB) / e->t);
	return (-1);
}

int		set_exectime(t_act *a, t_tval *t, t_env *e, int cs)
{
	int		carry;
	int		delay;
	int		usec;
	int		sec;

	delay = retrieve_delay(a->action, e, cs);
	if (delay == -1)
		delay = USEC_IN_SEC / (e->t * 2);
	sec = delay / USEC_IN_SEC;
	usec = delay % USEC_IN_SEC;
	carry = (t->tv_usec + usec) / USEC_IN_SEC;
	a->exec_time.tv_usec = (t->tv_usec + usec) % USEC_IN_SEC;
	a->exec_time.tv_sec = sec + carry + t->tv_sec;
	return (EX_SUCCESS);
}

void	set_actions(t_env *e, int cs, char **cmds)
{
	t_act	*tmp;
	t_tval	t;
	int		i;

	i = 0;
	if (e->fds[cs].act[0].exec_time.tv_sec == 0)
		gettimeofday(&t, NULL);
	else
	{
		while (e->fds[cs].act[i].exec_time.tv_sec != 0)
			i++;
		tval_cpy(&t, &e->fds[cs].act[i - 1].exec_time);
	}
	while (i < 10 && *cmds)
	{
		tmp = &e->fds[cs].act[i];
		tmp->action = *cmds;
		if (set_exectime(tmp, &t, e, cs) == EX_ERROR)
			return ;
		tval_cpy(&t, &tmp->exec_time);
		cmds++;
		i++;
	}
}

int		client_read(t_env *e, int cs)
{
	char	*s;
	char	**cmds;

	if ((s = p_recv(e, cs)) == NULL)
		return (EX_ERROR);
	cmds = ft_strsplit(s, '\n');
	free(s);
	if (cmds == NULL)
		return (send_error(cs, UNKNOWN_ERROR), EX_SUCCESS);
	set_actions(e, cs, cmds);
	free(cmds);
	return (EX_SUCCESS);
}
