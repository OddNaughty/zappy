/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   seefunc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/10 03:54:32 by cwagner           #+#    #+#             */
/*   Updated: 2014/06/10 03:54:32 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static char	*seeeast(t_env *e, t_fd *a)
{
	int		lvl;
	char	*saw;
	char	*col_content;
	int		col;

	lvl = 0;
	saw = ft_strdup("{");
	while (lvl <= a->level)
	{
		col = REALP((a->x + lvl), e->x);
		col_content = get_col(e, (a->y - lvl), col, (lvl * 2) + 1);
		saw = ft_strfreejoin(&saw, col_content);
		ft_strdel(&col_content);
		lvl++;
	}
	if (ft_strlen(saw))
	{
		saw[ft_strlen(saw) - 1] = '\0';
		saw[ft_strlen(saw) - 1] = '}';
	}
	return (saw);
}

static char	*seewest(t_env *e, t_fd *a)
{
	int		lvl;
	char	*saw;
	char	*col_content;
	int		col;

	lvl = 0;
	saw = ft_strdup("{");
	while (lvl <= a->level)
	{
		col = REALP((a->x - lvl), e->x);
		col_content = get_col_west(e, (a->y + lvl), col, (lvl * 2) + 1);
		saw = ft_strfreejoin(&saw, col_content);
		ft_strdel(&col_content);
		lvl++;
	}
	if (ft_strlen(saw))
	{
		saw[ft_strlen(saw) - 1] = '\0';
		saw[ft_strlen(saw) - 1] = '}';
	}
	return (saw);
}

static char	*seenorth(t_env *e, t_fd *a)
{
	int		lvl;
	char	*saw;
	char	*line_content;
	int		line;

	lvl = 0;
	saw = ft_strdup("{");
	while (lvl <= a->level)
	{
		line = REALP((a->y - lvl), e->y);
		line_content = get_line(e, (a->x - lvl), line, (lvl * 2) + 1);
		saw = ft_strfreejoin(&saw, line_content);
		ft_strdel(&line_content);
		lvl++;
	}
	if (ft_strlen(saw))
	{
		saw[ft_strlen(saw) - 1] = '\0';
		saw[ft_strlen(saw) - 1] = '}';
	}
	return (saw);
}

static char	*seesouth(t_env *e, t_fd *a)
{
	int		lvl;
	char	*saw;
	char	*line_content;
	int		line;

	lvl = 0;
	saw = ft_strdup("{");
	while (lvl <= a->level)
	{
		line = REALP((a->y + lvl), e->y);
		line_content = get_line_south(e, (a->x + lvl), line, (lvl * 2) + 1);
		saw = ft_strfreejoin(&saw, line_content);
		ft_strdel(&line_content);
		lvl++;
	}
	if (ft_strlen(saw))
	{
		saw[ft_strlen(saw) - 1] = '\0';
		saw[ft_strlen(saw) - 1] = '}';
	}
	return (saw);
}

int			see(t_env *e, t_fd *a)
{
	char	**split;
	char	*saw;
	char	*without_player;

	if ((split = ft_strsplitwhite(a->act[0].action)) == NULL)
		return (FAILURE);
	if (ft_strcmp(split[0], "voir") != SUCCESS)
		return (FAILURE);
	if (a->o == NORTH)
		saw = seenorth(e, a);
	else if (a->o == WEST)
		saw = seewest(e, a);
	else if (a->o == EAST)
		saw = seeeast(e, a);
	else
		saw = seesouth(e, a);
	if (!ft_strncmp(saw, "{joueur ", 8))
		asprintf(&without_player, "{%s", saw + 8);
	else
		asprintf(&without_player, "{%s", saw + 7);
	add_to_bufwrite(&(a->buf_write), without_player);
	ft_strdel(&saw);
	ft_strdel(&without_player);
	ft_freechartab(&split);
	return (SUCCESS);
}
