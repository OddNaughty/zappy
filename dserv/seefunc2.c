/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   seefunc2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/18 05:13:14 by cwagner           #+#    #+#             */
/*   Updated: 2014/06/18 05:13:14 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

char	*get_line_south(t_env *e, int x, int line, int nbr)
{
	char	*saw;
	char	*content;

	saw = NULL;
	while (nbr--)
	{
		if ((content = get_boxcontent(e->map[line][REALP(x, e->x)])) != NULL)
			saw = ft_strfreejoin(&saw, content);
		saw = ft_strfreejoin(&saw, ", ");
		x--;
	}
	return (saw);
}

char	*get_col_west(t_env *e, int y, int col, int nbr)
{
	char	*saw;
	char	*content;

	saw = NULL;
	while (nbr--)
	{
		if ((content = get_boxcontent(e->map[REALP(y, e->y)][col])) != NULL)
			saw = ft_strfreejoin(&saw, content);
		saw = ft_strfreejoin(&saw, ", ");
		y--;
	}
	return (saw);
}
