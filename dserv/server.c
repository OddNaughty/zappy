/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/21 05:20:25 by rgary             #+#    #+#             */
/*   Updated: 2014/06/21 05:20:25 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static void	init_fd(t_fd *fd)
{
	fd->buf_read = NULL;
	fd->buf_write = NULL;
}

void		init_env(t_env *e)
{
	int				i;
	struct rlimit	rlp;

	ft_bzero(e, sizeof(*e));
	e->port = STD_PORT;
	e->x = STD_WIDTH;
	e->y = STD_HEIGHT;
	e->nbcli = STD_NBCLI;
	e->t = STD_TIME;
	e->gameover = 0;
	X(-1, getrlimit(RLIMIT_NOFILE, &rlp), "getrlimit");
	e->maxfd = rlp.rlim_cur;
	e->fds = (t_fd*)XV(NULL, malloc(sizeof(*e->fds) * e->maxfd), "malloc");
	i = 0;
	while (i < e->maxfd)
	{
		init_fd(&e->fds[i]);
		clean_fd(&e->fds[i]);
		i++;
	}
}

void		srv_create(t_env *e, int port)
{
	int					s;
	struct sockaddr_in	sin;
	struct protoent		*pe;

	pe = (struct protoent*)XV(NULL, getprotobyname("tcp"), "getprotobyname");
	s = X(-1, socket(PF_INET, SOCK_STREAM, pe->p_proto), "socket");
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = INADDR_ANY;
	sin.sin_port = htons(port);
	X(-1, bind(s, (struct sockaddr*)&sin, sizeof(sin)), "bind");
	X(-1, listen(s, 42), "listen");
	e->fds[s].type = FD_SERV;
	e->fds[s].fct_read = srv_accept;
}

int			main(int ac, char **av)
{
	t_env	e;

	init_env(&e);
	if (get_opt(&e, ac, av) == EX_ERROR)
		return (fprintf(stderr, PSTR10), 0);
	srv_create(&e, e.port);
	initmap(&e);
	mainloop(&e);
	return (0);
}
