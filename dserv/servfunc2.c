/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   servfunc2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/09 06:38:18 by cwagner           #+#    #+#             */
/*   Updated: 2014/06/09 06:38:18 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

int		right(t_env *e, t_fd *a)
{
	char	**split;

	(void)e;
	if ((split = ft_strsplitwhite(a->act[0].action)) == NULL)
		return (FAILURE);
	if (ft_strcmp(split[0], STR_RIGHT) != SUCCESS)
		return (FAILURE);
	a->o = (a->o + 1) % 4;
	a->buf_write = ft_strdup("ok\n");
	gfxsend_pos(e->gfx_fd, a);
	ft_freechartab(&split);
	return (SUCCESS);
}

int		left(t_env *e, t_fd *a)
{
	char	**split;

	(void)e;
	if ((split = ft_strsplitwhite(a->act[0].action)) == NULL)
		return (FAILURE);
	if (ft_strcmp(split[0], STR_LEFT) != SUCCESS)
		return (FAILURE);
	a->o = (a->o + 3) % 4;
	a->buf_write = ft_strdup("ok\n");
	gfxsend_pos(e->gfx_fd, a);
	ft_freechartab(&split);
	return (SUCCESS);
}

char	*get_boxcontent(t_res *box)
{
	int			i;
	int			j;
	char		*saw;
	static char	*translate[NB_RESS] =

	{"nourriture", "linemate", "deraumere", "sibur", "mendiane", "phiras",
	"thystame"};
	saw = NULL;
	if (box->player)
	{
		i = box->player;
		while (i-- > 0)
			saw = ft_strfreejoin(&saw, "joueur ");
	}
	i = 0;
	while (i < NB_RESS)
	{
		j = box->ress[i];
		while (j-- > 0)
			asprintf(&saw, "%s ", ft_strfreejoin(&saw, translate[i]));
		i++;
	}
	if (ft_strlen(saw))
		saw[ft_strlen(saw) - 1] = '\0';
	return (saw);
}

char	*get_line(t_env *e, int x, int line, int nbr)
{
	char	*saw;
	char	*content;

	saw = NULL;
	while (nbr--)
	{
		if ((content = get_boxcontent(e->map[line][REALP(x, e->x)])) != NULL)
			saw = ft_strfreejoin(&saw, content);
		saw = ft_strfreejoin(&saw, ", ");
		x++;
	}
	return (saw);
}

char	*get_col(t_env *e, int y, int col, int nbr)
{
	char	*saw;
	char	*content;

	saw = NULL;
	while (nbr--)
	{
		if ((content = get_boxcontent(e->map[REALP(y, e->y)][col])) != NULL)
			saw = ft_strfreejoin(&saw, content);
		saw = ft_strfreejoin(&saw, ", ");
		y++;
	}
	return (saw);
}
