/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   srv_accept.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sbuono <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/12 14:22:12 by sbuono            #+#    #+#             */
/*   Updated: 2014/06/12 14:22:13 by sbuono           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void	check_for_eggs(t_env *e, int cs, t_team *t)
{
	t_egg	**tmp;
	t_egg	*tmp2;

	tmp = &e->egglist;
	while (*tmp)
	{
		if ((*tmp)->team == t && (*tmp)->hatched)
		{
			e->fds[cs].x = (*tmp)->x;
			e->fds[cs].y = (*tmp)->y;
			tmp2 = *tmp;
			*tmp = (*tmp)->next;
			free(tmp2);
			return ;
		}
		tmp = &((*tmp)->next);
	}
}

void	treat_client(t_env *e, int cs, char *s, t_team *t)
{
	char		*send;
	static int	id = 0;

	t = e->team;
	while (t)
	{
		if (!ft_strcmp(t->name, s) && ((t->nbmax_player - t->nb_player) > 0))
		{
			check_for_eggs(e, cs, t);
			e->fds[cs].team = t;
			e->fds[cs].type = FD_CLIENT;
			t->nb_player++;
			asprintf(&send, "%s\n%d %d\n", ft_itoa(TNBMP - TNBP), e->x, e->y);
			p_send(e, cs, send);
			free(send);
			gettimeofday(&e->fds[cs].next_life_loss, NULL);
			add_lifetime(e, &e->fds[cs].next_life_loss, &e->fds[cs].NLL);
			e->fds[cs].id = ++id;
			gfxsend_newplayer(e->gfx_fd, &e->fds[cs]);
			gfxsend_inventory(e->gfx_fd, &e->fds[cs]);
			return ;
		}
		t = t->next;
	}
	close_client(e, cs);
}

void	welcome(t_env *e, int cs)
{
	char	*s;

	p_send(e, cs, STR_WELCOME);
	if ((s = p_recv(e, cs)) == NULL)
		return ;
	s[ft_strlen(s) - 1] = 0;
	if (s == NULL)
		close_client(e, cs);
	if (!ft_strcmp(STR_GRAPHIC, s))
		treat_graphic(e, cs);
	else
		treat_client(e, cs, s, NULL);
	free(s);
}

int		srv_accept(t_env *e, int s)
{
	int					cs;
	struct sockaddr_in	csin;
	socklen_t			csin_len;

	csin_len = sizeof(csin);
	cs = X(-1, accept(s, (struct sockaddr*)&csin, &csin_len), "accept");
	printf("New client #%d from %s:%d\n", cs
		, inet_ntoa(csin.sin_addr), ntohs(csin.sin_port));
	clean_fd(&e->fds[cs]);
	e->fds[cs].fct_read = client_read;
	e->fds[cs].fct_write = client_write;
	srand(rand());
	e->fds[cs].x = rand() % e->x;
	srand(rand());
	e->fds[cs].y = rand() % e->y;
	srand(rand());
	e->fds[cs].o = rand() % 4;
	e->fds[cs].level = 1;
	e->fds[cs].inventory[FOOD] = 10;
	welcome(e, cs);
	e->map[e->fds[cs].y][e->fds[cs].x]->player++;
	return (EX_SUCCESS);
}
