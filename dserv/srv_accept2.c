/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   srv_accept.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sbuono <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/12 14:22:12 by sbuono            #+#    #+#             */
/*   Updated: 2014/06/12 14:22:13 by sbuono           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void	add_teams_to_buf(t_env *e)
{
	t_team		*t;
	char		*s;

	t = e->team;
	while (t)
	{
		asprintf(&s, "tna %s", t->name);
		add_to_bufwrite(&e->gfx_fd->buf_write, s);
		free(s);
		t = t->next;
	}
}

void	treat_graphic(t_env *e, int cs)
{
	char	*s;
	int		x;
	int		y;

	x = -1;
	e->fds[cs].type = FD_GFX;
	e->gfx_fd = e->fds + cs;
	asprintf(&s, "msz %d %d\nsgt %d", e->x, e->y, e->t);
	add_to_bufwrite(&e->gfx_fd->buf_write, s);
	free(s);
	while (++x < e->x)
	{
		y = -1;
		while (++y < e->y)
		{
			asprintf(&s, "bct %d %d %d %d %d %d %d %d %d", x, y, EMYX->ress[0]
			, EMYX->ress[1], EMYX->ress[2], EMYX->ress[3], EMYX->ress[4]
			, EMYX->ress[5], EMYX->ress[6]);
			add_to_bufwrite(&e->gfx_fd->buf_write, s);
			free(s);
		}
	}
	add_teams_to_buf(e);
}
