/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   time_tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sbuono <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/09 14:35:18 by sbuono            #+#    #+#             */
/*   Updated: 2014/06/09 14:35:19 by sbuono           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void	tval_cpy(t_tval *dest, t_tval *src)
{
	dest->tv_sec = src->tv_sec;
	dest->tv_usec = src->tv_usec;
}

int		tval_cmp(t_tval *now, t_tval *later)
{
	if (now->tv_sec != later->tv_sec)
		return (int)(later->tv_sec - now->tv_sec);
	else
		return (int)(later->tv_usec - now->tv_usec);
}

void	get_interval(t_tval *dest, t_tval *t1, t_tval *t2)
{
	dest->tv_sec = t1->tv_sec - t2->tv_sec;
	dest->tv_usec = t1->tv_usec - t2->tv_usec;
	if (dest->tv_sec < 0)
	{
		dest->tv_sec = -(dest->tv_sec);
		if (dest->tv_usec > 0)
		{
			dest->tv_usec -= USEC_IN_SEC;
			dest->tv_sec--;
		}
	}
	else if (dest->tv_sec > 0)
	{
		if (dest->tv_usec < 0)
		{
			dest->tv_usec += USEC_IN_SEC;
			dest->tv_sec--;
		}
	}
	dest->tv_usec = (dest->tv_usec > 0 ? dest->tv_usec : -(dest->tv_usec));
}
