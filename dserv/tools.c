/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sbuono <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/30 14:17:58 by sbuono            #+#    #+#             */
/*   Updated: 2014/05/30 14:17:59 by sbuono           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void	clean_fd(t_fd *fd)
{
	if (fd)
	{
		if (fd->buf_read)
			free(fd->buf_read);
		if (fd->buf_write)
			free(fd->buf_write);
		ft_bzero(fd, sizeof(*fd));
	}
}

int		x_int(int err, int res, char *str)
{
	if (res == err)
	{
		fprintf(stderr, "%s error: %s\n", str, strerror(errno));
		exit (1);
	}
	return (res);
}

void	*x_void(void *err, void *res, char *str)
{
	if (res == err)
	{
		fprintf(stderr, "%s error: %s\n", str, strerror(errno));
		exit (1);
	}
	return (res);
}

int		add_to_bufwrite(char **buf, char *add)
{
	if ((*buf = ft_strfreejoin(buf, add)) == NULL)
		return (FAILURE);
	if ((*buf = ft_strfreejoin(buf, "\n")) == NULL)
		return (FAILURE);
	return (SUCCESS);
}

void	freeswap(char **s1, char **s2)
{
	ft_strdel(s1);
	*s1 = ft_strdup(*s2);
	ft_strdel(s2);
}
