/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   updater.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sbuono <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/09 16:17:20 by sbuono            #+#    #+#             */
/*   Updated: 2014/06/09 16:17:21 by sbuono           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void	kill_client(t_env *e, int cs)
{
	int		i;
	int		x;
	int		y;

	i = -1;
	if (e->fds[cs].type == FD_CLIENT)
	{
		x = e->fds[cs].x;
		y = e->fds[cs].y;
		while (++i < NB_RESS)
			e->map[y][x]->ress[i] += e->fds[cs].inventory[i];
		e->fds[cs].team->nb_player--;
		e->map[y][x]->player--;
		gfxsend_content(e->gfx_fd, x, y, e->map[y][x]);
		p_send(e, cs, "mort\n");
		gfxsend_death(e->gfx_fd, e->fds[cs].id);
		close_client(e, cs);
	}
}

void	add_lifetime(t_env *e, t_tval *dest, t_tval *t)
{
	int		msec;

	msec = ((UT_BY_LIFE * USEC_IN_SEC) / e->t) + t->tv_usec;
	dest->tv_sec = t->tv_sec + (msec / USEC_IN_SEC);
	dest->tv_usec = msec % USEC_IN_SEC;
}

int		update_life(t_env *e, int cs, t_tval *t)
{
	if (tval_cmp(t, &e->fds[cs].next_life_loss) < 1)
	{
		e->fds[cs].inventory[FOOD]--;
		gfxsend_inventory(e->gfx_fd, &e->fds[cs]);
		add_ress(e, FOOD);
		if (e->fds[cs].inventory[FOOD] == -1)
		{
			kill_client(e, cs);
			return (EX_ERROR);
		}
		add_lifetime(e, &e->fds[cs].next_life_loss, t);
	}
	return (EX_SUCCESS);
}

void	update_nextupdate(t_env *e, int cs)
{
	if (e->next_update.tv_sec == 0)
		tval_cpy(&e->next_update, &e->fds[cs].next_life_loss);
	if (e->fds[cs].act[0].exec_time.tv_sec != 0
		&& (tval_cmp(&e->next_update, &e->fds[cs].act[0].exec_time) < 0))
		tval_cpy(&e->next_update, &e->fds[cs].act[0].exec_time);
	if (tval_cmp(&e->next_update, &e->fds[cs].next_life_loss) < 0)
		tval_cpy(&e->next_update, &e->fds[cs].next_life_loss);
}

void	update(t_env *e)
{
	int			i;
	t_tval		t;

	i = -1;
	while (++i < e->maxfd)
	{
		if (e->fds[i].type == FD_CLIENT)
		{
			gettimeofday(&t, NULL);
			if (update_life(e, i, &t) == EX_SUCCESS)
			{
				if (e->fds[i].act[0].exec_time.tv_sec != 0)
				{
					if (tval_cmp(&t, &e->fds[i].act[0].exec_time) < 1)
						update_action(e, i);
				}
			}
			update_nextupdate(e, i);
		}
	}
}
