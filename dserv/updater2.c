/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   updater2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sbuono <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/09 16:17:20 by sbuono            #+#    #+#             */
/*   Updated: 2014/06/09 16:17:21 by sbuono           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void	update_action(t_env *e, int cs)
{
	interpret(e, &e->fds[cs]);
	free(e->fds[cs].act[0].action);
	ft_memcpy((char*)&e->fds[cs].act[0], (char*)&e->fds[cs].act[1]
		, sizeof(t_act) * (ACT_MAX - 1));
	ft_bzero((char*)&e->fds[cs].act[9], sizeof(t_act));
}
