/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sbuono <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/30 17:09:24 by sbuono            #+#    #+#             */
/*   Updated: 2014/05/30 17:09:24 by sbuono           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

int		client_write(t_env *e, int cs)
{
	p_send(e, cs, e->fds[cs].buf_write);
	free(e->fds[cs].buf_write);
	e->fds[cs].buf_write = NULL;
	return (EX_SUCCESS);
}
