/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clse.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sbuono <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/30 11:26:22 by sbuono            #+#    #+#             */
/*   Updated: 2014/05/30 11:26:22 by sbuono           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLSE_H
# define CLSE_H

# include <stdlib.h>
# include <time.h>
# include <errno.h>
# include <stdio.h>
# include <string.h>
# include <sys/select.h>
# include <stdlib.h>
# include <sys/resource.h>
# include <stdlib.h>
# include <netdb.h>
# include <netinet/in.h>
# include <arpa/inet.h>
# include <sys/time.h>
# include "libft.h"

# define EX_ERROR			-1
# define EX_SUCCESS			0

# define IS_OK				0
# define UNKNOWN_ERROR		-1
# define TOO_MUCH_ACTIONS	1
# define ERR_WRONG_ACT		2

# define STR_WELCOME		"BIENVENUE\n"
# define STR_GRAPHIC		"GRAPHIC"

# define BUF_SIZE			4096

# define MAXLIFE			1260
# define ACT_MAX			10
# define USEC_IN_SEC		1000000

# define XV					x_void
# define X					x_int
# define MAX(x, y)			(x > y ? x : y)

typedef struct				s_send
{
	int						code;
	unsigned int			nbbytes;
}							t_send;

#endif
