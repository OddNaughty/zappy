/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gfx.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgary <rgary@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/09 11:37:34 by rgary             #+#    #+#             */
/*   Updated: 2014/06/09 11:37:34 by rgary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GFX_H
# define GFX_H

# include <mlx.h>
# include <sys/time.h>
# include "clse.h"
# include "libft.h"

# define ORIXV(err,res,str)	(x_void(err,res,str,__LINE__))
# define ORIX(err,res,str)	(x_int(err,res,str,__LINE__))

# define KEY_UP				65362
# define KEY_DOWN			65364
# define KEY_LEFT			65361
# define KEY_RIGHT			65363
# define KEY_ESC			65307
# define KEY_PLUS			65451
# define KEY_MINUS			65453

typedef struct s_gfxopt		t_gfxopt;
typedef struct addrinfo		t_addr;
typedef struct s_env		t_env;
typedef struct s_clist		t_clist;
typedef struct s_mlx		t_mlx;
typedef struct s_move		t_move;
typedef struct timeval		t_timeval;
typedef void				(*t_com)(t_env *e, char *str);

# define NB_RESS			7
# define NB_INFO			NB_RESS + 1
# define NB_C_INFO			NB_RESS + 4

# define EGG_OK				0
# define EGG_BROKEN			1
# define EGG_DEAD			2

# define BUF_SIZE			4096

# define ANIMTIME			(60 / ETU)

# define FOOD				0
# define LINEMATE			1
# define DERAUMERE			2
# define SIBUR				3
# define MENDIANE			4
# define PHIRAS				5
# define THYSTAME			6
# define INCANT				7
# define RESINC				8
# define ENDRESINC			9
# define BLOOD				10
# define EGG				11
# define BEGG				12
# define DEGG				13
# define ENDGAME			14
# define BDCT				15
# define TOTALIMG			16

# define SCREEN_WIDTH		768
# define SCREEN_HEIGHT		768
# define INFO_WIDTH			650
# define INFO_HEIGHT		768

# define CASE_LINE			0x000000ff
# define CFOOD				0x003B5323
# define CLINEMATE			0x0025383C
# define CDERAUMERE			0x00348781
# define CSIBUR				0x00FFFFCC
# define CMENDIANE			0x00EE9A4D
# define CPHIRAS			0x00483C32
# define CTHYSTAME			0x00F6358A
# define WHITE				0x00FFFFFF

# define INCANTSUCC			8
# define INCANTFAIL			9

/*
**DEFINE NORME
*/
# define SWW				SCREEN_WWIDTH
# define SWH				SCREEN_WHEIGHT
# define TLN				total_line_size
# define MNW				mlx_new_window
# define MNI				mlx_new_image
# define MXFTI				mlx_xpm_file_to_image
# define MGDA				mlx_get_data_addr
# define MPITW				mlx_put_image_to_window
# define MSP				mlx_string_put
# define MLX				e->mlx
# define SILI				size_line
# define XW					xpm_w
# define CBS				color_byte_size
# define ETU				e->time_unit
# define FDIMG				"imgs/food.xpm"
# define BCB				"imgs/bluecb.xpm"
# define GCB				"imgs/greencb.xpm"
# define GRCB				"imgs/greycb.xpm"
# define OCB				"imgs/orangecb.xpm"
# define RCB				"imgs/redcb.xpm"
# define VCB				"imgs/violetcb.xpm"
# define CCB				"imgs/cyancb.xpm"
# define INC				"imgs/incant.xpm"
# define INCS				"imgs/incantsuccess.xpm"
# define INCF				"imgs/incantfail.xpm"
# define BLD				"imgs/blood.xpm"
# define EGGIMG				"imgs/egg.xpm"
# define BROEGG				"imgs/brokenegg.xpm"
# define DEAEGG				"imgs/rottenegg.xpm"
# define ENDGAMEIMG			"imgs/endgame.xpm"
# define BDCTIMG			"imgs/broadcast.xpm"
# define SQ_HL				(e->screen_width / e->mszy)
# define SQ_VL				(e->screen_height / e->mszx)
# define INFO1				"Player %s | LVL | POS X | POS Y | ORI | Food | Lin"
# define INFO2				"e | Dera | Sibu | Mend | Phir | Thys | Team"
# define INFO3				INFO1 INFO2
# define PL1				"Player %3d | %3d | X:%3d | Y:%3d |  %c  |  %2d  | "
# define PL2				" %2d  |  %2d  |  %2d  |  %2d  |  %2d  |  %2d  | %s"
# define PL3				PL1 PL2
# define SCNB				save->client_nb
# define SES				save->egg_state
# define ESW				e->screen_width
# define ESH				e->screen_height
# define STR_WIN_LEN		((strlen(s[0]) / 2) * 5)

struct						s_move
{
	int						move_up;
	int						move_down;
	int						move_left;
	int						move_right;
};

struct						s_gfxopt
{
	char					*serv;
	int						port;
};

struct						s_clist
{
	int						client_nb;
	int						egg_nb;
	int						egg_state[2];
	int						color;
	int						inv[NB_RESS];
	int						bc[2];
	int						posx;
	int						posy;
	int						incant;
	int						ori;
	int						lvl;
	char					*teamname;
	t_clist					*next;
	t_clist					*prev;
};

struct						s_mlx
{
	void					*mlx;
	void					*win;
	void					*win2;
	void					*img;
	char					*draw;
	void					*fimg;
	char					*fdraw;
	void					**pic;
	char					**xpm;
	int						*xpm_w;
	int						*xpm_h;
	int						bpp;
	int						color_byte_size;
	int						size_line;
	int						total_line_size;
	int						endian;
};

struct						s_env
{
	int						max;
	int						sfd;
	int						screen_width;
	int						screen_height;
	int						img_posx;
	int						img_posy;
	float					zoom;
	fd_set					fd_read;
	fd_set					fd_write;
	int						dead[3];
	int						time_unit;
	int						mszx;
	int						mszy;
	int						***res;
	int						gameover;
	int						rdy;
	char					*winner;
	t_move					move;
	t_mlx					*mlx;
	t_clist					*clist;
};

/*
**client loop
*/
void						set_env(t_env *e, int sfd);
void						main_gfx_loop(int sfd);

/*
**cmd edi sgt seg smg suc
*/
void						treat_edi(t_env *e, char *cmd);
void						treat_sgt(t_env *e, char *cmd);
void						treat_seg(t_env *e, char *cmd);
void						treat_smg(t_env *e, char *cmd);
void						treat_suc(t_env *e, char *cmd);

/*
**cmd msz bct tna ppo plv
*/
void						treat_msz(t_env *e, char *cmd);
void						treat_bct(t_env *e, char *cmd);
void						treat_tna(t_env *e, char *cmd);
void						treat_ppo(t_env *e, char *cmd);
void						treat_plv(t_env *e, char *cmd);

/*
**cmd pdr pgt eht ebo
*/
void						treat_pdr(t_env *e, char *cmd);
void						treat_pgt(t_env *e, char *cmd);
void						treat_eht(t_env *e, char *cmd);
void						treat_ebo(t_env *e, char *cmd);

/*
**cmd pex pbc pic pie pfk
*/
void						treat_pex(t_env *e, char *cmd);
void						treat_pbc(t_env *e, char *cmd);
void						treat_pic(t_env *e, char *cmd);
void						treat_pie(t_env *e, char *cmd);
void						treat_pfk(t_env *e, char *cmd);

/*
**command treatment
*/
void						free_args(char **args);
void						fill_res(t_env *e);
void						treat_sbp(t_env *e, char *cmd);

/*
**draw arms
*/
void						draw_r_left_arm(t_env *e, int i, int j, int color);
void						draw_d_left_arm(t_env *e, int i, int j, int color);
void						draw_r_right_arm(t_env *e, int i, int j, int color);
void						draw_d_right_arm(t_env *e, int i, int j, int color);

/*
**draw background
*/
int							draw_horizontal_line(int i, t_env *e);
void						draw_vertical_line(int i, t_env *e);
void						draw_grass(int i, t_env *e);
void						draw_background(t_env *e);

/*
**draw broadcast
*/
void						draw_broadcast(t_env *e);

/*
**draw eggs
*/
void						draw_eggs(t_env *e);

/*
**draw endgame
*/
void						draw_winner(t_env *e);
void						draw_endgame(t_env *e);

/*
**draw incants
*/
void						draw_incants(t_env *e);
void						draw_incant(t_env *e, int line, int j, int c);
void						draw_incant_result(t_env *e, int l, int j, int c);

/*
**draw infos
*/
void						reset_page(t_env *e);
int							fill_ori(t_clist *save);
void						draw_infos(t_env *e);

/*
**draw it
*/
void						draw_it(t_env *e);

/*
**draw legs
*/
void						draw_ls_legs(t_env *e, int i, int j, int color);
void						draw_rs_legs(t_env *e, int i, int j, int color);
void						draw_lf_legs(t_env *e, int i, int j, int color);
void						draw_rf_legs(t_env *e, int i, int j, int color);

/*
**draw player
*/
void						draw_head(t_env *e, int i, int j, int color);
void						draw_body(t_env *e, int i, int j, int color);
void						draw_left_neck(t_env *e, int i, int j, int color);
void						draw_right_neck(t_env *e, int i, int j, int color);
void						draw_players(t_env *e);

/*
**draw ressources
*/
void						draw_ressources(t_env *e);
void						draw_ress_squares(t_env *e, int i, int j);
int							find_start(t_env *e, int start, int ress);
void						draw_img(t_env *e, int start, int c);

/*
**gfx
*/
int							socket_and_connect(t_addr *info);
int							treat_options(int argc);
void						ft_set_data(t_addr *data);
int							main(int argc, char **argv);

/*
**handle imgs
*/
int							insert_img_3(t_env *e);
int							insert_img_2(t_env *e);
int							insert_img(t_env *e);

/*
**handle mlx build
*/
int							expose_hook(t_env *e);
int							loop_hook(t_env *e);
int							key_press_event(int keycode, t_env *e);
int							key_release_event(int keycode, t_env *e);
int							create_mlx(t_env *e);

/*
**launch init
*/
t_com						*fill_com_func_2(t_com *f);
t_com						*fill_com_func(int nb);
void						treat_cmd(t_env *env, char *cmd);
void						treat_recv_infos(t_env *e, char *str);
void						first_init(t_env *e, int sfd);

/*
**protocol gfx
*/
int							g_send(t_env *e, int sfd, char *s);
char						*g_recv(t_env *e, int cs);

/*
**tools
*/
int							generate_color(void);
int							zoom(t_env *e, int way);
void						call_to_loop(t_env *e);
void						draw_player_one(t_env *e, t_clist *save);
void						draw_player_two(t_env *e, t_clist *save);

/*
**treat enw
*/
t_clist						*insert_eggs_in_client_list(t_env *e, char **args);
void						treat_enw(t_env *e, char *cmd);

/*
**treat pdi
*/
void						treat_pdi(t_env *e, char *cmd);

/*
**treat pin
*/
void						replace_inv(t_clist *user, char **args, t_env *e);
void						treat_pin(t_env *e, char *cmd);

/*
**treat pnw
*/
t_clist						*insert_info_in_client_list(t_env *e, char **args);
void						treat_pnw(t_env *e, char *cmd);

/*
**x
*/
int							x_int(int err, int res, char *str, int line);
void						*x_void(void *err, void *res, char *str, int line);

#endif

