/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sbuono <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/30 12:14:40 by sbuono            #+#    #+#             */
/*   Updated: 2014/05/30 12:14:41 by sbuono           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SERVER_H
# define SERVER_H

# include "clse.h"

# define STR_PORT			"-p"
# define STR_WIDTH			"-x"
# define STR_HEIGHT			"-y"
# define STR_TEAM			"-n"
# define STR_NBCLI			"-c"
# define STR_TIME			"-t"
# define OPT_NONE			0
# define OPT_PORT			1
# define OPT_WIDTH			2
# define OPT_HEIGHT			3
# define OPT_TEAM			4
# define OPT_NBCLI			5
# define OPT_TIME			6

# define STD_PORT			9999
# define STD_WIDTH			5
# define STD_HEIGHT			5
# define STD_NBCLI			5
# define STD_TIME			100

# define STD_MAXPLAYER		5

# define MIN_WINSIZE		5

# define FD_FREE			0
# define FD_SERV			1
# define FD_CLIENT			2
# define FD_GFX				3

# define UT_BY_LIFE			126

# define STR_ADVANCE		"avance"
# define STR_RIGHT			"droite"
# define STR_LEFT			"gauche"
# define STR_SEE			"voir"
# define STR_INVENTORY		"inventaire"
# define STR_TAKE			"prend"
# define STR_DROP			"pose"
# define STR_EXPULSE		"expulse"
# define STR_CAST			"broadcast"
# define STR_SPELL			"incantation"
# define STR_FORK			"fork"
# define STR_CON_NB			"connect_nbr"

/*
**NORME
*/
# define PSTR				"Alors moi je suis le joueur %d\nMa team peut "
# define PSTR2				"avoir %d joueurs et nous sommes %d\nCoos: y: %d x:"
# define PSTR3				" %d o: %d\n"
# define PSTR4				PSTR PSTR2 PSTR3
# define PSTR5				"{nourriture %d, linemate %d, deraumere %d, sibur "
# define PSTR6				"%d, mendiane %d, phiras %d, thystame %d}"
# define PSTR7				PSTR5 PSTR6
# define PSTR8				"Usage: ./serveur -p <port> -x <width> -y <height>"
# define PSTR9				" -n <team> [<team>] [<team>] ... -c <nb> -t <t>\n"
# define PSTR10				PSTR8 PSTR9
# define PSTR11				"bct %d %d %d %d %d %d %d %d %d"
# define NLL				next_life_loss
# define EMYX				e->map[y][x]
# define TNBMP				t->nbmax_player
# define TNBP				t->nb_player

# define UT_ADVANCE			7
# define UT_RIGHT			7
# define UT_LEFT			7
# define UT_SEE				7
# define UT_INVENTORY		1
# define UT_TAKE			7
# define UT_DROP			7
# define UT_EXPULSE			7
# define UT_CAST			7
# define UT_SPELL			300
# define UT_FORK			42
# define UT_HATCH			600
# define UT_ROT				1260
# define UT_CON_NB			0

# define NB_RESS			7
# define NB_ACT				12
# define NB_LVLUP			7

# define FOOD				0
# define LINEMATE			1
# define DERAUMERE			2
# define SIBUR				3
# define MENDIANE			4
# define PHIRAS				5
# define THYSTAME			6

# define RATIO_FOOD			(100.0 / 100.0)
# define RATIO_COMMON		(30.0 / 100.0)
# define RATIO_RARE			(10.0 / 100.0)
# define RATIO_LEGENDARY	(1.0 / 100.0)

# define NB_FOOD(x, y)		(int)((float)x * (float)y * (float)RATIO_FOOD)
# define NB_CRESS(x, y)		(int)((float)x * (float)y * (float)RATIO_COMMON)
# define NB_RRESS(x, y)		(int)((float)x * (float)y * (float)RATIO_RARE)
# define NB_LRESS(x, y)		(int)((float)x * (float)y * (float)RATIO_LEGENDARY)
# define REALP(pos, max)	(pos < 0) ? ((pos % max) + max) : (pos % max)

# define NORTH				0
# define EAST				1
# define SOUTH				2
# define WEST				3

typedef struct timeval		t_tval;

typedef struct				s_res
{
	int						ress[NB_RESS];
	int						player;
}							t_res;

typedef struct				s_act
{
	t_tval					exec_time;
	char					*action;
}							t_act;

typedef struct				s_team
{
	char					*name;
	int						nb_player;
	int						nbmax_player;
	int						level8;
	struct s_team			*next;
}							t_team;

typedef struct				s_egg
{
	int						id;
	t_team					*team;
	t_tval					hatch;
	t_tval					rot;
	int						x;
	int						y;
	int						hatched;
	struct s_egg			*next;
}							t_egg;

typedef struct				s_fd
{
	int						type;
	int						id;
	int						(*fct_read)();
	int						(*fct_write)();
	char					*buf_read;
	char					*buf_write;
	int						inventory[NB_RESS];
	t_team					*team;
	int						x;
	int						y;
	int						o;
	int						inc_x;
	int						inc_y;
	t_act					act[ACT_MAX];
	t_tval					next_life_loss;
	int						level;
}							t_fd;

typedef struct				s_env
{
	t_fd					*fds;
	t_fd					*gfx_fd;
	int						port;
	int						maxfd;
	int						max;
	int						r;
	fd_set					fd_read;
	fd_set					fd_write;
	int						x;
	int						y;
	int						nbcli;
	int						t;
	int						gameover;
	t_res					***map;
	t_team					*team;
	t_tval					next_update;
	t_egg					*egglist;
}							t_env;

/*
** TOOLS
*/
void						clean_fd(t_fd *fd);
int							x_int(int err, int res, char *str);
void						*x_void(void *err, void *res, char *str);
int							add_to_bufwrite(char **buf, char *add);
void						freeswap(char **s1, char **s2);

/*
** GETOPT
*/
int							checkopt(char *s);
int							getoptnumbers(t_env *e, int opt, char *av);
int							getwinsize(t_env *e, int opt, char *av);
int							getteams(t_env *e, char *av);
void						set_team_maxplayer(t_env *e);
int							get_opt(t_env *e, int ac, char **av);

/*
** MAINLOOP
*/
void						mainloop(t_env *e);

/*
** SRV_ACCEPT
*/
void						treat_graphic(t_env *e, int cs);
void						add_teams_to_buf(t_env *e);
int							srv_accept(t_env *e, int s);

/*
** WRITE
*/
int							client_write(t_env *e, int cs);

/*
** READ
*/
int							client_read(t_env *e, int cs);

/*
** UPDATER
*/
void						update_action(t_env *e, int cs);
void						update(t_env *e);
void						add_lifetime(t_env *e, t_tval *dest, t_tval *t);
void						kill_client(t_env *e, int cs);

/*
** INITMAP
*/
void						initmap(t_env *e);
void						add_ress(t_env *e, int ress);
void						init_ressources(t_env *e);
void						add_nress(t_env *e, int ress, int n);

/*
** TIME_TOOLS
*/
void						tval_cpy(t_tval *dest, t_tval *src);
int							tval_cmp(t_tval *now, t_tval *later);
void						get_interval(t_tval *dest, t_tval *t1, t_tval *t2);

/*
** PROTOCOL_TOOLS
*/
void						close_client(t_env *e, int cs);
void						send_error(int cs, int errorcode);
void						send_ok(int cs);
int							p_send(t_env *e, int cs, char *s);
char						*p_recv(t_env *e, int cs);

/*
** INCAFUNC
*/
int							incantation(t_env *e, t_fd *a);
int							checksend_incantation(t_env *e, t_fd *a);

/*
** INCAFUNC2
*/
void						treat_levels(t_fd *other, t_env *e, int i);
int							result_from_if(int *ids, int j, t_fd *a, t_fd *o);
void						stone_explode(t_env *e, t_res *box, int level);

/*
** INTERPRET
*/
void						interpret(t_env *e, t_fd *a);

/*
** INVENTORYFUNC
*/
int							inventory(t_env *e, t_fd *a);

/*
** MOVEFUNC
*/
int							move(t_env *e, t_fd *a);

/*
** SERVFUNC2
*/
int							right(t_env *e, t_fd *a);
int							left(t_env *e, t_fd *a);
char						*get_boxcontent(t_res *box);
char						*get_line(t_env *e, int x, int line, int nbr);
char						*get_col(t_env *e, int y, int col, int nbr);

/*
** SEEFUNC
*/
int							see(t_env *e, t_fd *a);

/*
** SEEFUNC2
*/
char						*get_line_south(t_env *e, int x, int line, int nbr);
char						*get_col_west(t_env *e, int x, int line, int nbr);

/*
** GETFUNC
*/
void						fill_translate(char *translate[NB_RESS]);
int							get(t_env *e, t_fd *a);

/*
** PUTFUNC
*/
int							put(t_env *e, t_fd *a);

/*
** EXPULSEFUNC
*/
int							expulse(t_env *e, t_fd *a);

/*
** EGGFUNC
*/
int							egg(t_env *e, t_fd *a);

/*
** COFUNC
*/
int							connectnbr(t_env *e, t_fd *a);

/*
** BROADFUNC
*/
int							broadcast(t_env *e, t_fd *a);

/*
** GFXSEND1
*/
void						gfxsend_expulse(t_fd *gfx, int id);
void						gfxsend_pos(t_fd *gfx, t_fd *a);
void						gfxsend_ik(t_fd *gfx, t_fd *a, int level, int *ids);
void						gfxsend_inc(t_fd *gfx, t_fd *a, int success);
void						gfxsend_lvlup(t_fd *gfx, int id, int level);

/*
** GFXSEND2
*/
void						gfxsend_content(t_fd *gfx, int x, int y, t_res *b);
void						gfxsend_inventory(t_fd *gfx, t_fd *a);
void						gfxsend_get(t_fd *gfx, t_res *b, t_fd *a, int res);
void						gfxsend_put(t_fd *gfx, t_res *b, t_fd *a, int res);
void						gfxsend_newplayer(t_fd *gfx, t_fd *a);

/*
** GFXSEND3
*/
void						gfxsend_broad(t_fd *gfx, int id, char *msg);
void						gfxsend_death(t_fd *gfx, int id);
void						gfxsend_playerlay(t_fd *gfx, int id);
void						gfxsend_egglay(t_fd *gfx, int id, t_egg *egg);
void						gfxsend_eggrot(t_fd *gfx, t_egg *egg);
void						gfxsend_egghatch(t_fd *gfx, t_egg *egg);
void						gfxsend_victory(t_fd *gfx, char *msg);

#endif
